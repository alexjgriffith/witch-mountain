love.conf = function(t)
   t.title, t.identity = "witch-mountain", "Witch Mountain"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 768 -- 128 x 6
   t.window.height = 576 -- 96 x 6
   t.window.vsync = true
   t.version = "11.3"
   t.window.resizable=false
end
