;; tiles -> dense
;; objects

;; 16 base encode
;; 0123456789abcdef
;;ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=
;; 1 0101
;; 2 012012
;; 3 01230123
;; 24 bit alignment (6 bits per char) i.e. blocks of 4
(local base64-padding "=")
(local base64-alphabet
       "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")

(fn base10to64 [base10]
  (var core base10)
  (var ret "")
  (while (>= core 1)
    (let [child (% core 64)]
      (set core (math.floor (/ core 64)))
      (set ret (.. (string.sub base64-alphabet (+ child 1) (+ child 1))
                   ret))))
  (local pad# (% (- 4 (% (length ret) 4)) 4))
  (for [k 1 pad#]
    (set ret (.. ret base64-padding)))
  ret)

;; (baseXto10 1111111111111111111111111111 2) ;; @ 27 bits int breaks
;; (baseHto10 [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1] 2) ;; @ 46 bits int breaks

;; 64 = 2^6 -> 2^(* 6 4) 2^24 - limit a block to 24 bits 2 -> 24, 4 -> 20 8 -> 16 16 -> 12 32 -> 8 64 -> 4 128 -> 2 256 -> 1

;; 1 block = 64^4
;; x sections -> dec -> 1 block
(fn baseXto10 [value base]
  (var core value)
  (var i 0)
  (var ret 0)
  (while (>= core 1)
    (let [child (% core 10)]      
      (set core (math.floor (/ core 10)))
      (set ret (+ ret (* child (^ base i))))
      (set i (+ i 1))))
  ret)

(fn baseHto10 [value base]
  (var ret 0)
  (var reverse [])
  (var l (length value))
  (each [i child (ipairs value)]
    (tset reverse (+ 1 (- l i)) child))
  ;; (pp reverse) 
  (each [i child (ipairs reverse)]
    (set ret (+ ret (* child (^ base (- i 1))))))
  ret)


;; (fn baseHto64 [value base]
;;   (var ret 0)
;;   (var reverse [])
;;   (var l (length value))
;;   (var remainder 0)
;;   (each [i child (ipairs value)]
;;     (tset reverse (+ 1 (- l i)) child))
;;   (each [i child (ipairs reverse)]
;;     (set ret (+ ret (* child (^ base i))))
;;     (if (> ret 16777216)
        
;;         )
;;     )
;;   ret)


;; (* 64 64 64 64)
;; 16777216

(fn baseHto10R [value base]
  (var ret 0)
  (var reverse [])
  (var l (length value))
  (each [i child (ipairs value)]
    (set ret (+ ret (* child (^ base i)))))
  ret)

(fn encodeRow [row base]
  (-> row (baseHto10R base) (base10to64)))

(fn encodeTiles [tiles base]
  ;; CCRRBXX
  ;; 64, 4096 262144 16777216
  (local cols (length tiles))
  (local rows (* (length (.tiles 1)) (^ 64 2)))
  (local info (base10to64 (+ cols rows)))
  )

;; (local ground
;;        {:w
;;         :h
;;         :tiles [[] []]
;;         :packed ""
;;         :tileset })
