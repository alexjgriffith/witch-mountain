Title: Seasonal Platformer Tiles 
Author: GrafxKid
Link: https://opengameart.org/content/seasonal-platformer-tiles
Licence: http://creativecommons.org/publicdomain/zero/1.0/legalcode" CC0
Modified: Yes

Title: Snwoy Cottage
Author: TAD
Link: https://opengameart.org/content/snowy-cottage-8bit-ver2
Licence: https://creativecommons.org/publicdomain/by/3.0/legalcode" CCBY4.0
Modified: No

Title: 8 Bit Sound Effects Library
Author: Little Robot Sound Factory
Link: https://opengameart.org/content/8-bit-sound-effects-library
Licence: https://creativecommons.org/publicdomain/by/3.0/legalcode" CCBY3.0
Modified: No

Title: Variety of Cursors
Author: Kemono
Link: https://opengameart.org/content/variety-of-cursors
Licence: https://creativecommons.org/publicdomain/by/3.0/legalcode" CCBY3.0
Modified: No

Title: Jump Landing Sound
Author: MentalSanityOff
Link: https://opengameart.org/content/jump-landing-sound
Licence: http://creativecommons.org/publicdomain/zero/1.0/legalcode" CC0
Modified: Yes

Title: Funny Comic Cartoon Bounce Sound
Author: Blender Foundation
Link: https://opengameart.org/content/funny-comic-cartoon-bounce-sound
Licence: https://creativecommons.org/publicdomain/by/3.0/legalcode" CCBY3.0
Modified: No
