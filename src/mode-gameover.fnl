(local gameover {})
(local fade (require :lib.fade))
(local resources (require :resources))
(local witch {:ox -10 :oy 0 :flip -1})
(var rate 2)
(local gamestate (require :lib.gamestate))

(fn gameover.enter []
  (resources.birds:setVolume 0.6)
  (fade.in))

(fn gameover.leave []
  (resources.birds:setVolume 0))

(fn gameover.draw []
  (love.graphics.push)
  (love.graphics.scale 6)
  (love.graphics.setColor 1 1 1 1)
  (resources.title-screen:draw "Green Background")  
  (local star-image (. resources.stars :Star1 :image))
  (local star1 (. resources.stars :Star1 :animation))
  (local star2 (. resources.stars :Star2 :animation))
  (local star3 (. resources.stars :Star3 :animation))
  (star1:draw star-image 100 0)
  (star2:draw star-image 66 16)
  (star3:draw star-image 66 16)
  (resources.title-screen:draw :Mountain)
  (resources.title-screen:draw "Green")
  (resources.title-screen:draw "GameOver")
  (love.graphics.draw resources.blossom-particles 128 -96)
  (resources.title-screen:draw :Witch witch.ox witch.oy)  
  (love.graphics.setColor 1 1 1 1)  
  (love.graphics.draw fade.canvas)  
  (love.graphics.pop))

(fn gameover.update [self dt]
    (set witch.oy (+ witch.oy (* rate dt)))
  (if (> witch.oy 5)
      (set rate -2);; use flux
      (< witch.oy 0)
      (set rate 2))

  (local star1 (. resources.stars :Star1 :animation))
  (local star2 (. resources.stars :Star2 :animation))
  (local star3 (. resources.stars :Star3 :animation))
  (star1:update dt)
  (star2:update dt)
  (star3:update dt))

(fn gameover.keypressed [self key code]
  (match key
    :escape (gamestate.push (require :mode-dropdown))))

gameover
