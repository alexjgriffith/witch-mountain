(local stone {})
(local world (. (require :state) :col))

(local lg love.graphics)

(fn stone.draw [self image frozen?]
  ;;(pp frozen?)
  (local obj (. resources :atlas (if frozen? self.sprite self.altsprite)))
  ;; (lg.setColor 1 0 0 0.3)
  ;; (lg.rectangle :fill self.col.x self.col.y
  ;;               self.col.w self.col.h)
  (lg.setColor 1 1 1 1)
  (love.graphics.draw obj.image obj.quad self.x self.y)  
  )

(fn stone.interact [self player]
  (when (or (= 0 self.time) (= self.period self.time))    
    (set self.active (not self.active))    
    (each [key value (ipairs self.linked)]
      (: value (if self.active :link-on :link-off) value))))

(fn stone.update [self dt]
  (local rate (* dt 5))
  (if self.active
      (do (set self.time (math.min self.period (+ self.time rate))))
      (do (set self.time (math.max 0 (- self.time rate))))))

(fn stone.serialize [self] self.brush)

(fn stone.link [self obj]
  (table.insert self.linked obj))

(local stone-mt {:__index stone})

(fn stone.preview [brush]
  (local {: x : y  : name : sprite : altsprite} brush)
  (setmetatable
          {:brush brush
           : name
           : sprite
           : altsprite
           :x x           
           :y y
           :col {:type :trigger :name :stone :x (+ x 0) :y (+ y 16) :w 16 :h 16}
           } stone-mt))

(fn stone.create [brush]
  (local {: x : y  : name : sprite : altsprite} brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           : altsprite
           :active false
           :range 70
           :period 2
           :time 0
           :w 16
           :h 16
           :x x                      
           :y y
           :linked []
           :col {:type :trigger :name :stone :x (+ x 0) :y (+ y 16) :w 16 :h 16}
           } stone-mt))
  (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
  obj
  )

stone


