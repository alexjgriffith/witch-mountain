(fn clone-animation [image animation]
  {: image :animation (animation:clone)})

(fn clone-animation-table [t]
  (var ret {})
  (each [key {: image : animation} (pairs t)]
    (tset ret key (clone-animation image animation)))
  ret)

{: clone-animation-table : clone-animation}
