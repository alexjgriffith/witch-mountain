(local house {})

(local lg love.graphics)

(fn house.remove [self])

(fn house.draw [self image frozen?]
  ;;(pp frozen?)
  (local obj (. resources :atlas (if frozen? self.sprite self.altsprite)))
  (love.graphics.draw obj.image obj.quad self.x self.y))

(fn house.update [self dt])

(fn house.serialize [self] self.brush)

(fn house.preview [brush]
  (house.create brush))

(local house-mt {:__index house})

(fn house.create [brush]
  (local {: x : y  : name : sprite : altsprite} brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           : sprite
           : altsprite
           :w 16
           :h 16
           :x x
           :y y} house-mt))
  obj
  )

house
