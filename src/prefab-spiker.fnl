(local spiker {})
(local world (. (require :state) :col))
(local object (require :object))
(setmetatable spiker {:__index object})
(local lg love.graphics)

(local {: clone-animation} (require :util))

(local q-map {:l 1 :tl 2 :t 3 :tr 4 :r 5 :br 6 :b 7 :bl 8})

(fn quads [self]
  (local {: x : y : w : h} self.col)
  [{:x (- x w) :y (+ y 0) :w w :h h :n :l :a math.pi}
   {:x (- x w) :y (- y h) :w w :h h :n :tl :a (* math.pi (/ 3 4))}
   {:x (- x 0) :y (- y h) :w w :h h :n :t :a (* math.pi (/ 2 4))}
   {:x (+ x w) :y (- y h) :w w :h h :n :tr :a (* math.pi (/ 1 4))}
   {:x (+ x w) :y (- y 0) :w w :h h :n :r :a 0}
   {:x (+ x w) :y (+ y h) :w w :h h :n :br :a (* math.pi (/ -1 4))}
   {:x (+ x 0) :y (+ y h) :w w :h h :n :b :a (* math.pi (/ -2 4))}
   {:x (- x w) :y (+ y h) :w w :h h :n :bl :a (* math.pi (/ -3 4))}
   ])

(fn sense [self]
  (local player (. ( require :state) :player))
  (local q (quads self))
  (local hits [])
  (local dists {:ground (+ 1 self.w)})
  (local angles [])
  (each [_ {: x : y : w : h : a : n} (ipairs q)]
    (local (col len) (world:queryRect x y w h (fn [item] (= item.name :ground))))
    (when (> len 0)
      (when (= n :b)
        (set dists.ground (- (. col 1 :y) y)))
      (table.insert angles a)
      (table.insert hits n)))
  (local (len _) (world:querySegment (+ player.col.x 4)
                                     (+ player.col.y 4)
                                     (+ self.col.x 4)
                                     self.col.y
                                     (fn [item] (= item.name :ground))))
  (local player-visible (= len 0))
  (values player-visible hits
          dists
          angles
          q
          [(+ player.col.x 4)
           (+ player.col.y 4)
           (+ self.col.x 4)
           self.col.y]))

(fn calculate-velocity-gravity [dy vx dx]
  (let [vy (/ (* 2 dy vx) dx)
        g (/ (* -2 dy (* vx vx)) (* dx dx))]
    (values vy g)))

(fn move-up-down [posy vel acc dt]
  (let [posy-next (+ posy (*  vel dt) (* 0.5 (- acc) dt dt))
        vel-next (+ vel (* (- acc) dt))]
    (values posy-next vel-next)))

(local x-velocity-max 0.2)

(local (_ g) (calculate-velocity-gravity 30 (* 60 x-velocity-max) 16))

(local sm {:fall {} :move {}})
(fn sm.fall.enter [self _from]
  (set self.state :fall))
(fn sm.fall.update [self dt posx posy]
  (let [vel self.y-velocity
        (posy-next vel-next) (move-up-down posy vel g dt)]
    (set self.y-velocity vel-next)
    (set self.x-velocity 0)
  (values posx posy-next)))
(fn sm.fall.leave [self to]
  (when to ((. sm to :enter) self :fall)))
(fn sm.fall.inputs [self _direction toground _hits _angs _player-visible]
  (sm.fall.leave self (if (<= toground 0) :move)))

(fn sm.move.enter [self _from]
  (set self.state :move))
(fn sm.move.update [self dt posx posy]
  (set self.y-velocity 0)
  (local dir (if (= self.direction :left) -1 1))
  (set self.x-velocity (* dir x-velocity-max))
  (values (+ self.x-velocity posx) posy))
(fn sm.move.leave [self to]
  (when to ((. sm to :enter) self :move)))
(fn sm.move.inputs [self direction _toground hits _angs _player-visible]
  (fn member [el ar]
    (var ret false)
    (each [_ v (ipairs ar)]
      (when (= el v)
        (set ret true)))
    ret)
  (match direction
    :left (when (or (member :l hits) (not (member :bl hits)))
            (set self.direction :right))
    :right (when (or (member :r hits) (not (member :br hits)))
             (set self.direction :left))))


(fn spiker.debug-draw [self]
  (lg.setColor 1 0 0 0.3)
  (lg.rectangle :fill self.col.x self.col.y
                self.col.w self.col.h)
  (each [_ h (ipairs self.hits)]
    (local {: x : y : w : h} (. self.q (. q-map h)))
    (lg.rectangle :fill x y w h))
  (lg.line self.line)
  (lg.setColor 1 1 1 1)
  (lg.setFont resources.textFont)
  (lg.print (..  self.state) (- self.col.x 10) (- self.col.y 15))
  (lg.print (..  self.toground) (- self.col.x 10) (- self.col.y 5))  
)

(fn spiker.draw [self image]
  (local {: x : y} self)
  (local s self.animation)
  (lg.setColor 1 1 1 1)
  (when self.active
    (s.animation:draw s.image x y))
  )

(fn spiker.first [self]
  (object.setup-link self))

(fn spiker.link-on [self]
  (set self.active false))

(fn spiker.link-off [self]
  (set self.active true))

(fn spiker.update [self dt]
  (self:first)
  (when self.active    
    (local (player-visible hits dists _angles q line) (sense self))
    (set self.q q)
    (set self.hits hits)
    (set self.line line)
    (set self.toground dists.ground)
    ((. sm self.state :inputs) self self.direction dists.ground
   hits _angles player-visible)
  (local (px py) ((. sm self.state :update) self dt self.col.x self.col.y))
  (local (ax ay cols len) (world:move self.col px py (fn [item other]
                                              ;; (pp other)
                                              ;;(pp item)
                                              (match other.name
                                                :ground :slide
                                                :player :cross
                                                _ false))))
  (when (> len 0)
    (each [_ col (ipairs cols)]
      (when (= :player col.other.name)
        (love.event.push :player-hit :spiker (world:hasItem self.col))
      )
    ))
  ;; (do
  ;;   ) false)
  (set self.col.x ax)
  (set self.col.y ay)
  (set self.x (- ax 4))
  (set self.y (- ay 8))
  (local s self.animation)
  (s.animation:update (* 0.5 dt))))

(fn spiker.serialize [self] self.brush)

(local spiker-mt {:__index spiker})

(fn spiker.preview [brush]
  (local {: x : y  : name} brush)
  (local obj
         (setmetatable
          {: name
           :direction :right
           :y y
           :x x
           :animation (clone-animation resources.characters.Spiker.image
                                       resources.characters.Spiker.animation)
           :active true
           :col {:type :enemy :name :spiker :x (+ x 4) :y (+ y 8) :w 8 :h 8}
           } spiker-mt)
  ))

(fn spiker.create [brush]
  (local {: x : y  : name} brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           :state :fall
           :direction :right
           :animation (clone-animation resources.characters.Spiker.image
                                       resources.characters.Spiker.animation)
           :x-velocity 0
           :y-velocity 0
           :active true
           :w 16
           :h 16
           :y y
           :x x
           :has-run false
           :col {:type :enemy :name :spiker :x (+ x 4) :y (+ y 8) :w 8 :h 8}
           } spiker-mt))
  (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
  obj
  )

spiker
