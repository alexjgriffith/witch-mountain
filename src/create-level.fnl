(local atlas :/assets/sprites/atlas.png)
(local level (require :lib.editor))
(local state (require :state))
(local bump (require :lib.bump))
(local world (bump.newWorld))
(tset state :col world)
(local objects (require :objects))
(local object-creation-callbacks (objects:get-create))
(local object-preview-callbacks (objects:get-preview))

(fn replace-tile [layer-name brush-name x y {:x w :y h}]
  (local (sx sy) (values (* (- x 1) w) (* (- y 1) h)))
  (local obj {:name brush-name :layer layer-name
              :type :tile :x sx :y sy : w : h})
  (local (items len) (world:queryPoint
                      (+ sx 1) (+ sy 1)
                      (fn [item]
                        (and (= item.type :tile)
                             (= item.layer layer-name)))))
  (when (> len 0)
    (world:remove (. items 1)))
  (when (and (= :ground layer-name) (= :ground brush-name))
    (world:add obj sx sy w h)))

(fn remove-object [name obj]
  ((. objects name :remove) obj)
  )

(fn love.handlers.edit-level [key layer-name brush-name x y tilesize?]
  ;; (pp [key layer-name brush-name x y tilesize?])
  (match key
    :replace-tile (replace-tile layer-name brush-name x y tilesize?)
    :add-object :nil
    :remove-object (remove-object brush-name x)
    ))

(local editor level.editor)
(local brush editor.brush)

(tset brush :brushes  [[:ground :ground :tile]
                       [:ground :chasm :tile]
                       [:objects :player :object]
                       [:objects :spiker :object]
                       [:objects :flower :object]
                       [:objects :witch :object]
                       [:bgobjects :house :object]
                       [:bgobjects :tree :object]
                       [:bgobjects :bush :object]
                       [:bgobjects :stone :object]
                       [:bgobjects :chest :object]
                       [:bgobjects :pot :object]
                       [:bgobjects :fence :object]
                       [:bgobjects :snow :object]
                       [:bgobjects :smalltree :object]
                       [:bgobjects :s :object]
                       [:bgobjects :v :object]
                       [:bgobjects :w :object]
                       [:bgobjects :ww :object]
                       ])

(tset brush :count 16)

(tset brush :index 1)

(fn editor.keypressed [key code]
  (match key
    :g (brush.set 1)
    :c (brush.set 2)
    :p (brush.set 3)
    :h (brush.set 4)
    :f (brush.set 5)
    "[" (brush.decrement)
    "]" (brush.increment)))


;; (local ground-brushes
;;        {:ground {:name :ground
;;                  :bitmap :bitmap-3x3-simple-alt :bitmap-w 4
;;                  :char :1 :ix 0 :iy 0}
;;         :chasm {:name :chasm
;;                 :bitmap :bitmap-1 :bitmap-w 1
;;                 :char :0 :ix 0 :iy 11}})

;; (local object-brushes
;;        {:player {:name :player}})

;; (local l1 (level.create
;;            :l1 514 514
;;            16
;;            atlas
;;            object-creation-callbacks))

(local l1 (level.load-map :assets.levels.game-map object-creation-callbacks object-preview-callbacks))

(fn rebrush [map-data layer-name]
  (local layer (. map-data :layers layer-name))
  (local data (. layer :data))
  (local brushes (. layer :brushes))
  (local callbacks (. map-data :object-creation-callbacks))
  (var new-data [])
  (each [i d (ipairs data)]
    (local {: x : y : name} d)
    (local brush (lume.clone (. brushes name)))
    (set brush.x x)
    (set brush.y y)
    (tset new-data i ((. callbacks name) brush))
    )
  (set layer.data new-data)
  map-data)

;; (rebrush l1 :bgobjects)

(set l1.layers.altground.encoded-data l1.layers.ground.encoded-data)

(editor.init l1 {:x 0 :y 0 :scale 4})
(set state.player (. l1.layers.objects.data 1))
;; (-> l1
;;     (l1.add-layer :ground :tile ground-brushes)
;;     (l1.add-layer :objects :objects object-brushes)
;;     (l1.add :ground 2 6 :ground true)
;;     (l1.add :ground 3 6 :ground true)
;;     (l1.add :ground 4 6 :ground true)
;;     (l1.add :ground 5 6 :ground true)
;;     (l1.add :ground 6 6 :ground true)
;;     (l1.add :ground 7 6 :ground true)
;;     (l1.add :ground 8 6 :ground true)
;;     (l1.add :ground 8 5 :ground true)
;;     (l1.add :ground 8 4 :ground true)
;;     (l1.add :ground 8 4 :ground true)
;;     (l1.add :ground 4 3 :ground true)
;;     (l1.add :ground 5 3 :ground true)
;;     (l1.add :objects 8 0 :player true)
;;     )

l1
