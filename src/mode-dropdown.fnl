(local dropdown {})

(local resources (require :resources))
(local lg love.graphics)

(local fade (require :lib.fade))
(local gamestate (require :lib.gamestate))

(fn dropdown.enter [self from]
  (love.mouse.setVisible true)
  (set self.from from)
  )

(fn dropdown.leave [self]
  (when (= :game self.from.name)
    (love.mouse.setVisible false)
    ))

(var mute false)

(var click-down false)
(var hovered false)
(fn click [self text]
  (local down (love.mouse.isDown 1))
  (if down
      (when (not click-down)
        (resources.beep:stop)
        (resources.beep:play)
        (match text
          :Menu (fade.out (fn []
                            (gamestate.pop)
                            (gamestate.switch (require :mode-menu))))
          :Reset (fade.out (fn []
                             (lume.hotswap  :create-level)
                             (gamestate.pop)
                             (gamestate.switch (require :mode-menu))))
          :Creds (fade.out (fn []                             
                             (gamestate.pop)
                             (gamestate.switch (require :mode-credits))))
          :Mute (set mute (not mute))
          :Back (gamestate.pop)
          )
        (set click-down true)
        )
      (do
        (when (~= text hovered)
          (do (set hovered text)
              (resources.click:stop)
              (resources.click:play)))
        (set click-down false)))
  )

(fn dropdown.draw [self]
  (self.from.draw)
  (local {: quad : image} resources.ui.Menu)
  (local {:quad dbQuad :image dbImage} resources.ui.DarkButton)
  (local {:quad lbQuad :image lbImage} resources.ui.LightButton)
  (local {:quad dsQuad :image dsImage} resources.ui.DarkSmall)
  (local {:quad muteQuad} resources.ui.IconMute)
  (local {:quad unmuteQuad} resources.ui.IconUnmute)
  (local {:quad xQuad} resources.ui.IconX)
  (local {:quad lsQuad} resources.ui.LightSmall)
  (local (x y) (love.mouse.getPosition))
  (local (mx my) (values (/ x 6) (/ y 6)))
  (local (bw bh) (values (* 48 1) (* 16 1)))
  (fn draw-text [text x y]
    (lg.setColor resources.black)
    (lg.printf text (+ x 2) (+ y 1) (- bw 4) :center)
    (lg.setColor 1 1 1 1))
  (lg.push)  
  (lg.scale 6)
  (lg.draw image quad 40 8)
  (lg.setFont resources.buttonFont)
  (let [x 40 y 15 text :Menu]
    (if (pointWithin mx my x y bw bh)
        (do (lg.draw lbImage lbQuad x y)
            (click self text)
            (draw-text text x y))
        (do (lg.draw dbImage dbQuad x y)
            (draw-text text x (- y 1)))))
  (let [x 40 y 35 text :Reset]
    (if (pointWithin mx my x y bw bh)
        (do (lg.draw lbImage lbQuad x y)
            (click self text)
            (draw-text text x y))
        (do (lg.draw dbImage dbQuad x y)
            (draw-text text x (- y 1)))))
  (let [x 40 y 55 text :Creds]
    (if (pointWithin mx my x y bw bh)
        (do (lg.draw lbImage lbQuad x y)
            (click self text)
            (draw-text text x y))
        (do (lg.draw dbImage dbQuad x y)
            (draw-text text x (- y 1)))))
  (if (pointWithin mx my 48 71 bh bh)
      (do (lg.draw image lsQuad 48 71)
          (click self :Mute)
          (lg.draw image (if mute unmuteQuad muteQuad) (+ 4 48) (+ 4 71)))
      (do (lg.draw image dsQuad 48 71)
          (lg.draw image (if mute unmuteQuad muteQuad) (+ 4 48) (+ 3 71))))
  (if (pointWithin mx my 64 71 bh bh)
      (do (lg.draw image lsQuad 64 71)
          (lg.draw image xQuad (+ 4 64) (+ 4 71))
          (click self :Back))
      (do (lg.draw image dsQuad 64 71)
          (lg.draw image xQuad (+ 4 64) (+ 3 71))))
  (lg.draw fade.canvas)
  (lg.pop)
  )

(fn dropdown.update [self dt]
  (if mute
      (love.audio.setVolume 0)
      (love.audio.setVolume 1))
  (self.from:update dt))

(fn dropdown.keypressed [self key code]
  (match code
    :escape (gamestate.pop)))

dropdown
