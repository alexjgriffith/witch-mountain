(local player {})
(local world (. (require :state) :col))
(local object (require :object))
(setmetatable player {:__index object})
(local lg love.graphics)
(fn player.serialize [self] self.brush)

(fn player.draw-debug [self]
  (local {: x : y} self)
  (love.graphics.push)
  (love.graphics.setColor 1 0 0 1)
  (love.graphics.rectangle :fill (+ x 8) (+ y 16) 16 16)
  (local py (+ self.col.y self.col.h))
  (local px (+ self.col.x self.col.w))
  (love.graphics.setColor 1 0 1 1)
  (love.graphics.rectangle :fill self.col.x py self.col.w 13)
  (love.graphics.rectangle :fill px self.col.y 13 self.col.h)
  (love.graphics.rectangle :fill self.col.x self.col.y -13 self.col.h)  
  (love.graphics.pop)
  (love.graphics.print self.state 2 2))

(fn player.draw [self]    
  (local s (. resources.player self.state))
  (local {: x : y : state} self)
  (var cx 0.2)
  ;; (lg.setColor 1 0 0 0.3)
  ;; (lg.rectangle :fill self.col.x self.col.y
  ;;               self.col.w self.col.h)
  (lg.setColor 1 1 1 1)
  (when (= :Dash state)
    (each [i previous (ipairs self.last-x)]
      (love.graphics.setColor (- 1 (* i cx 0.75)) 0 (+ 0 (* i cx 0.5)) 0.7)      
      (s.animation:draw s.image (+ previous 0) (+ 8 y))
      ))
  (if (> self.respawn-timer 0)
      (let [mod (/ (% (* 50 self.respawn-timer) 10) 20)]
        (love.graphics.setColor (- 1 mod) (- 1 mod) 0 1))
      (love.graphics.setColor 1 1 1 1))
  (s.animation:draw s.image (+ x 0) (+ 8 y)))

(fn love.handlers.player-hit [by-what col]
  (local player (. (require :state) :player))
  (pp [:player-hit col] )
  (when (<= player.respawn-timer 0)
    (set player.dead true)))


(fn love.handlers.flower-picked []
  (local player (. (require :state) :player))  
  (set player.flowers (+ player.flowers 1)))

(fn love.handlers.player-change-state [state]
  (match state
    :Jump (resources.jump:play)
    :Dash (resources.dash:play)
    :Land (resources.land:play)))

(fn state-change-signal [previous next]
  (love.event.push
   :player-change-state
   (match (values previous next)
     (_ :Jump) :Jump
     (_ :Dash) :Dash
     (:Fall :Run) :Land
     (:Fall :Run) :Land
     (:Jump :Run) :Land
     (:Jump :Run) :Land
     _ next
     )))

(fn interact [self]
  (local {: x : y : w : h} self.col)
  (local state (require :state))
  (local (items len) (world:queryRect x y w h (fn [item] (= :trigger item.type))))  
  (local order {:witch 1 :flower 2 :stone 3})  
  (local s (or (love.keyboard.isDown :s) (love.keyboard.isDown :down)))
  (if state.message.active
      (when (and s (not player.interact))
        (set state.message.i (+ state.message.i 1))
        (when (or (not state.message.text)
                  (> state.message.i (# state.message.text)))
          (when state.message.callback (state.message.callback))
          (set state.message.active false))
        )
      (when (and s (not player.interact) (> len 0))
        (var interaction nil)
        (each [_ item (ipairs items)]
          (each [_ d (ipairs (. state :level :layers :bgobjects :data))]
            (when (and d.col
                       (= item.x d.col.x) (= item.y d.col.y) (= item.name d.col.name))
              (when (or (not interaction) (> (. order interaction.name)
                                             (. order d.name)))
                (set interaction d))
              ))
          (each [_ d (ipairs (. state :level :layers :objects :data))]
            (when (and d.col
                       (= item.x d.col.x) (= item.y d.col.y) (= item.name d.col.name))
              (when (or (not interaction) (> (. order interaction.name)
                                             (. order d.name)))
                (set interaction d))
              ))
          )
        (when interaction
          (resources.beep:play)
          (interaction:interact self))))
  
  (if (not s)
      (set player.interact false)
      (set player.interact true)))

(fn player.translate [self x y]
  (world:update self.col x y)
  (set self.col.x x)
  (set self.col.y y))

(fn player.set-respawn [self]
  (set self.find-good-respawn true))

(fn respawn [player]
  (set player.dead false)
  (resources.click:play)
  (set player.respawn-timer 1)
  (player:translate player.spawn-point.x player.spawn-point.y)
  )

(fn bird-sounds [self]
  (local level (. (require :state) :level))
  (local bgobjs (. level :layers :bgobjects :data))
  (fn dist2 [{:col {:x x1 :y y1}} {:col {:x x2 :y y2}}]
    (+ (^ (- x1 x2) 2) (^ (- y1 y2) 2)))
  (var distance (* 100 100))
  (each [_ value (ipairs bgobjs)]
      (when (and (= value.active true) (= value.name :stone))
        (local d (dist2 self value))
        (when (or (not distance) (< d distance))
          (set distance d))
        )
      )
  (if (< distance (* 70 70))
      (resources.birds:setVolume 0.6)
      (< distance (* 100 100))
      (resources.birds:setVolume (* 0.6 (/ (- (* 100 100) distance) (* 100 100))))
      (resources.birds:setVolume 0)
    )
  )

(fn fire-sounds [self]
  (local level (. (require :state) :level))
  (local bgobjs (. level :layers :objects :data))
  (fn dist2 [{:col {:x x1 :y y1}} {:col {:x x2 :y y2}}]
    (+ (^ (- x1 x2) 2) (^ (- y1 y2) 2)))
  (var distance (* 100 100))
  (each [_ value (ipairs bgobjs)]
      (when (= value.name :witch)
        (local d (dist2 self value))
        (when (or (not distance) (< d distance))
          (set distance d))
        )
      )
  (if (< distance (* 30 30))
      (resources.fire:setVolume 0.2)
      (< distance (* 70 70))
      (resources.fire:setVolume (* 0.2 (/ (- (* 100 100) distance) (* 100 100))))
      (resources.fire:setVolume 0)
    )
  )

(fn player.update [self dt]
  (local state (require :state))
  (bird-sounds self)
  (fire-sounds self)
  (when state.message.active (set self.state :Idle))
    (var s (. resources.player self.state))
  (when (not state.message.active)
    (local movement (require :movement))
    (local (_ ax ay direction Mstate onground) (movement self world dt))
    (when (and onground self.find-good-respawn)
      (set self.find-good-respawn false)
      (set self.spawn-point.x self.col.x)
      (set self.spawn-point.y self.col.y))
    (set self.direction direction)
    (local state (match Mstate
                   :idle :Idle
                   :fall :Fall
                   :jump :Jump
                   :walk :Run
                   :wall :DashUp
                   :dash :Dash
                   _ :Idle))    
    (when (~= self.state state)
      (state-change-signal self.state state)
      (s.animation:gotoFrame 1)
      (set s (. resources.player state))
      (set self.state state))  
    (if (= direction :left)
        (set s.animation.flippedH true)
        (set s.animation.flippedH false))  
    (set self.state state)  
    (set self.x (- ax 14));;(+ ax 8))
    (table.insert self.last-x 1 self.x)
    (tset self.last-x 6 nil)
    (set self.y (- ay 28));;(+ ay 16))    
    (when  (or (> self.y 530) self.dead)
      (respawn self)))
  (interact self)
  (set self.respawn-timer (- self.respawn-timer dt))
  (s.animation:update dt)
  )

(local player-mt {:__index player})

(fn player.preview [brush]
  (local {: x : y} brush)
  (local p 
         {:name :player
          :state :Idle
          :x x
          :y y
          :col {:type :object :name :player :x (+ x 14) :y (+ y 28) :w 4 :h 4}
          })
  (setmetatable p player-mt))

(fn player.create [brush]
  (local {: x : y} brush)
  (local p 
         {:brush brush
          :name :player
          :state :Idle
          :x x
          :y y
          :w 32
          :h 32
          :dead false
          :start-pos {:x x :y y}
          :last-x []
          :flowers 0
          :respawn-timer 0
          :col {:type :object :name :player :x (+ x 14) :y (+ y 28) :w 4 :h 4}
          :spawn-point {:x (+ x 14) :y (+ y 28)}})
  (world:add p.col (+ p.x p.col.x) (+ p.x p.col.y) p.col.w p.col.h)
  (setmetatable p player-mt))

player
