(local ap (require :lib.aseprite-parser))
(local {: hex-to-rgba} (require :lib.colour))

;; Particles
(local snow (love.graphics.newImage "assets/sprites/SnowParticle.png"))
(local snow-particles (love.graphics.newParticleSystem snow 4000))
(snow-particles:setParticleLifetime 5)
(snow-particles:setEmissionRate 400)
(snow-particles:setDirection (+ 0.3 (/ math.pi 2)))
(snow-particles:setEmissionArea :uniform (* 4 128) (* 4 96))
(snow-particles:setSpeed 50 80)


(local blossom (love.graphics.newImage "assets/sprites/BlossomParticle.png"))
(local blossom-particles (love.graphics.newParticleSystem blossom 4000))
(blossom-particles:setParticleLifetime 5)
(blossom-particles:setEmissionRate 400)
(blossom-particles:setDirection (+ 0.3 (/ math.pi 2)))
(blossom-particles:setEmissionArea :uniform (* 5 128) (* 5 96))
(blossom-particles:setSpeed 20 30)

;; Cursor
(local cursorImage (love.graphics.newImage "assets/sprites/CleanCursor.png"))
(local cursor (love.mouse.newCursor "assets/sprites/CleanCursor-export.png"))

;; Tiles
(local tile-set
       {:chasm {:layer :ground :pos [1 5] :size :square4 :auto :blob :collidable :fall}
        :ground {:layer :ground :pos [12 1] :size :square10 :auto :blob :collidable :fall}})
(local tile-order [:chasm :ground])

(local
 resources
   {:title-screen (ap.layers-as-quads "assets/data/TitleScreen.json")
    :clean-tiles (ap.layers-slices-as-quads "assets/data/CleanTiles.json")
    :atlas (ap.slices-as-quads "assets/data/atlas.json")
    :tile-sheet (love.graphics.newImage "assets/sprites/CleanTiles.png")
    :characters (ap.tags-as-animations "assets/data/Characters.json")    
    :player (ap.tags-as-animations "assets/data/Player.json")
    :flowers (ap.tags-as-animations "assets/data/Flowers.json")
    :yellowflower (ap.tags-as-animations "assets/data/YellowFlower.json")
    :ui (ap.slices-as-quads "assets/data/UI.json")
    :snow snow
    :cursorImage cursorImage
    :cursor cursor 
    :stars (ap.tags-as-animations "assets/data/Stars.json")
    :titleFont (love.graphics.newFont "assets/fonts/vermin_vibes_1989/Vermin Vibes 1989.ttf" 16)
    :textFont (love.graphics.newFont "assets/fonts/pico8/pico-8.otf" 6)
    :buttonFont (love.graphics.newFont "assets/fonts/free_pixel/FreePixel.ttf" 16)
    :bgm (love.audio.newSource "assets/music/snowy-cottage.mp3" (if web
                                                                    "static"
                                                                    "stream"))
    :fire (love.audio.newSource "assets/sounds/fire.ogg" (if web
                                                            "static"
                                                            "stream"))
    :birds (love.audio.newSource "assets/sounds/birds.ogg" (if web
                                                              "static"
                                                              "stream"))
    :white (hex-to-rgba "#faf8f0")
    :black (hex-to-rgba "#1a1010")
    :dark-sky (hex-to-rgba "#4f3f71")
    :light-sky (hex-to-rgba "#886493")
    :beep (love.audio.newSource "assets/sounds/8bit/Collect_Point_00.mp3" "static")
    :click (love.audio.newSource "assets/sounds/8bit/Menu_Navigate_03.mp3" "static")
    :jump (love.audio.newSource "assets/sounds/bounce.ogg" "static")
    :dash (love.audio.newSource "assets/sounds/bounce.ogg" "static")    
    :land (love.audio.newSource "assets/sounds/land.ogg" "static")
    :snow-particles snow-particles
    :blossom-particles blossom-particles
    :tile-set tile-set
    :tile-order tile-order
    })  
  (love.graphics.setBackgroundColor resources.black)
  (love.filesystem.setIdentity "Witch Mountain")
(resources.bgm:play)
(resources.land:setVolume 0.2)
  (resources.bgm:setLooping true)
(resources.bgm:setVolume 0.25)
  (resources.beep:setVolume 0.25)
  (resources.click:setVolume 0.1)

resources
