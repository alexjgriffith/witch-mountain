(local flower {})
(local world (. (require :state) :col))
(local object (require :object))
(setmetatable flower {:__index object})
(local lg love.graphics)

(local {: clone-animation-table} (require :util))

(fn flower.draw [self image]
  (local {: x : y} self)
  (local s (. self.animations self.state))
  (lg.setColor 1 1 1 1)
  (when self.active
    (s.animation:draw s.image x y)))

(fn flower.first [self]
  (object.setup-link self))

(fn flower.link-on [self]
  (when self.not-picked
    (set self.col.type :trigger)
    (set self.active true)))

(fn flower.link-off [self]
  (set self.col.type :off)
  (set self.active false))

(fn flower.update [self dt]
  (self:first)
  (when self.active
    (local s (. self.animations self.state))
    (s.animation:update dt))
  )

(fn flower.interact [self player]
  (when self.not-picked
    (love.event.push :flower-picked)
    (set self.active false)
    (set self.col.type :off)
    (set self.not-picked false)))

(local flower-mt {:__index flower})

(fn flower.preview [brush]
  (local {: x : y  : name} brush)
  (local obj
         (setmetatable
          {: name
           :y y
           :x x
           :active true
           :animations (clone-animation-table resources.yellowflower)
           :col {:type :flower :name :flower :x (+ x 4) :y (+ y 8) :w 8 :h 8}
           } flower-mt)
         ))

(fn flower.create [brush]
  (local {: x : y  : name} brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           :state :Idle
           :active false
           :animations (clone-animation-table resources.yellowflower)
           :w 16
           :h 16
           :y y
           :x x
           :has-run false
           :not-picked true
           :col {:type :off :name :flower :x x :y y :w 16 :h 16}
           } flower-mt))
  (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
  obj
  )

flower


