(fn calculate-velocity-gravity [dy vx dx]
  (let [vy (/ (* 2 dy vx) dx)
        g (/ (* -2 dy (* vx vx)) (* dx dx))]
    (values vy g)))

(local x-velocity-max 1)

(local (vy g) (calculate-velocity-gravity 30 (* 60 x-velocity-max) 16))

(local t (/ (* 2 30) vy))

(local jump-max 2)
(local dash-max 1)
(local wall-max 1)
(local jump-time t)
(local dash-time 0.15)
(local toground-range 12)
(local time-to-max-ground 0.1)
(local time-to-max-air 0.2)
(local jump-velocity (* -1 vy))
(local coyote-time 0.15)
(local jump-g (* 1 1 g))
(local rise-g (* 1 1.2 g))
(local fall-g (* 1 1.1 g))
(local dash-velocity (* x-velocity-max 5))
(local slide-speed 0)


(var jump-down false)
(var dash-down false)

(local sm {:idle {}
           :walk {}
           :jump {}
           :fall {}
           :dash {}
           :wall {}
           :x-velocity 0
           :y-velocity 0
           :direction :right
           :angle 0
           :x-down false
           :state :idle
           :air-time 0
           :jump-count 0
           :jump-timer 0
           :dash-timer 0           
           :dash-count 0
           :wall-count 0})

(fn move-left-right [x-velocity x-down direction time-to-max dt]    
  (local dir (match direction :left -1 :right 1))
  (local rate (/ x-velocity-max time-to-max))
  (* dir (if x-down
             (math.min
              (* 60 dt x-velocity-max)
              (+ (math.abs sm.x-velocity) (* dt rate)))
             (math.max
              0
              (- (math.abs sm.x-velocity) (* dt rate))))))

(fn move-up-down [posy vel acc dt]
  (let [posy-next (+ posy (*  vel dt) (* 0.5 (- acc) dt dt))
        vel-next (+ vel (* (- acc) dt))]
    (values posy-next vel-next)))

(fn sm.idle.enter [from]
  (set sm.state :idle)
  (set sm.air-time 0)
  (set sm.dash-count 0)
  (set sm.wall-count 0)
  (set sm.jump-count 0))
(fn sm.idle.update [dt posx posy]
  (set sm.x-velocity 0)
  (set sm.y-velocity 0)
  (values posx posy))
(fn sm.idle.leave [to]
  (when to ((. sm to :enter) :idle)))
(fn sm.idle.inputs [toground x-velocity dash jump towall]
  (sm.idle.leave
   (if (> jump 0) :jump
       (> toground 0) :fall
       (> dash 0) :dash
       (and (> (math.abs towall) 0) (or sm.x-down (~= 0 x-velocity))) :walk)))

(fn sm.walk.enter [from]
  (set sm.state :walk)
  (set sm.air-time 0)
  (set sm.dash-count 0)
  (set sm.wall-count 0)
  (set sm.jump-count 0))
(fn sm.walk.update [dt posx posy]  
  (set sm.x-velocity
       (move-left-right sm.x-velocity sm.x-down sm.direction time-to-max-ground dt)) 
  (set sm.y-velocity 0)
  (values (+ posx sm.x-velocity) posy))
(fn sm.walk.leave [to]
  (when to ((. sm to :enter) :walk)))
(fn sm.walk.inputs [toground x-velocity dash jump]
  (sm.walk.leave
   (if (> jump 0) :jump
       (> dash 0) :dash
       (= x-velocity 0) :idle
       (> toground 0) :fall)))

(fn sm.jump.enter [from]  
  (when (< sm.jump-count jump-max)
    (set sm.state :jump)
    (when (= :wall from)
      (set sm.x-velocity (+ (* 2 x-velocity-max))))
    (set sm.y-velocity jump-velocity)
    (set sm.jump-count (+ sm.jump-count 1))))
(fn sm.jump.update [dt posx posy]  
  (set sm.air-time (+ sm.air-time dt))
  (set sm.jump-timer (- sm.jump-timer dt))  
  (let [vel sm.y-velocity
        (posy-next vel-next) (move-up-down posy vel jump-g dt)]
    (set sm.y-velocity vel-next)
  (set sm.x-velocity
       (move-left-right sm.x-velocity sm.x-down sm.direction time-to-max-air dt))
  (values (+ posx sm.x-velocity) posy-next)))
(fn sm.jump.leave [to]
  (when to (set sm.jump-timer 0))
  (when to ((. sm to :enter) :jump)))
(fn sm.jump.inputs [toground x-velocity dash jump towall]  
  (sm.jump.leave
   (if (= toground 0) :walk
       ;; (and sm.x-down (= towall 0)) :wall
       (<= jump 0) :fall
       (> dash 0) :dash)))

(fn sm.fall.enter [from]
  (set sm.state :fall))
(fn sm.fall.update [dt posx posy]  
  (set sm.air-time (+ sm.air-time dt))
  (when (and (= sm.jump-count 0) (> sm.air-time coyote-time))
    (set sm.jump-count 1))  
  (let [vel sm.y-velocity
        acc (if (< vel 0) rise-g fall-g)
        (posy-next vel-next) (move-up-down posy vel acc dt)]
    (set sm.y-velocity vel-next)
    (set sm.x-velocity
         (move-left-right sm.x-velocity sm.x-down sm.direction time-to-max-air dt))
  (values (+ posx sm.x-velocity) posy-next)))
(fn sm.fall.leave [to]
  (when to ((. sm to :enter) :fall)))
(fn sm.fall.inputs [toground x-velocity dash jump towall]
  (when (and (< toground toground-range) (< toground sm.y-velocity))
    (set sm.jump-count 0))
  (sm.fall.leave
   (if (= toground 0) :walk
       (> jump 0) :jump
       (> dash 0) :dash
       (and sm.x-down (= towall 0)) :wall)))

(fn sm.dash.enter [from]
  (set sm.state :dash)  
  ;;(set sm.x-velocity (* dash-velocity (math.cos sm.angle)))
  ;;(set sm.y-velocity (* dash-velocity (math.sin sm.angle)))  
  (do (local dir (if (= sm.direction :left) -1 1))
      (set sm.dash-count (+ sm.dash-count 1))
      (set sm.y-velocity 0)
      (set sm.x-velocity (* dir dash-velocity) )))
(fn sm.dash.update [dt posx posy]
  (set sm.dash-timer (- sm.dash-timer dt))  
  (values (+ posx sm.x-velocity) (+ posy (* dt 60 sm.y-velocity))))
(fn sm.dash.leave [to]
  (when to (set sm.x-velocity 0))
  (when to ((. sm to :enter) :dash)))
(fn sm.dash.inputs [toground x-velocity dash jump]  
  (sm.dash.leave (if (and (<= dash 0) (= 0 toground)) :walk
                     (<= dash 0) :fall)))

(fn sm.wall.enter [from]
  (if (< sm.wall-count wall-max)
      (do (set sm.state :wall)
          (set sm.wall-count (+ sm.wall-count 1))
          (set sm.air-time 0)
          (set sm.dash-count 0)
          (set sm.jump-count -1))
      (sm.wall.leave from)))
(fn sm.wall.update [dt posx posy]
  (set sm.x-velocity 0)
  (set sm.y-velocity slide-speed)
  (values posx (+ posy sm.y-velocity)))
(fn sm.wall.leave [to]
  (when to
    (set sm.direction (if (= :left sm.direction) :right :left))
    ((. sm to :enter) :wall)))
(fn sm.wall.inputs [toground x-velocity dash jump towall]
  (sm.wall.leave
   (if (> jump 0) :jump
       (> dash 0) :dash
       (= sm.down-down true) :fall
       ;; (or (not sm.x-down) (> (math.abs towall) 0)) :fall
       (= toground 0) :idle)))


(fn distance-to-nearest-ground-tile [player world ground-filter]
  (local py (+ player.col.y player.col.h))
  (local (cols len) (world:queryRect player.col.x
                                    py
                                    player.col.w
                                    toground-range
                                    ground-filter))  
  (var max-dist (+ 1 toground-range))  
  (each [_ c (ipairs cols)]
    (local dist (- py c.y))
    (when (< dist max-dist)
      (set max-dist dist)))
  max-dist)

(fn distance-to-nearest-wall-tile [player world direction ground-filter]
  (local grab-height 8)
  (local py (- player.col.y player.col.h grab-height))
  (local px (+ player.col.x player.col.w))
  (local (cols len)
         (if (= :right direction)
             (world:queryRect px
                              py
                              toground-range
                              2
                              ground-filter)
             (world:queryRect (- player.col.x toground-range)
                              py
                              toground-range
                              2
                              ground-filter)
             ))
  (var max-dist (+ 1 toground-range))  
  (each [_ c (ipairs cols)]
    (local dist (if (= :right direction) (- px c.x) (- (+ c.x c.w) player.col.x)))    
    (when (< dist max-dist)
      (set max-dist dist)))
  max-dist)

(fn ground-filter [item]  
  (and (= item.layer :ground) (= item.name :ground)))

;; controler
;; keyboard inputs
;; physics inputs
;; inputs
;; update
;; update physics
(local isDown love.keyboard.isDown)
(fn update [player world dt]
  (let [left (or (isDown :left) (isDown :a))
        right (or (isDown :right) (isDown :d))
        up (or (isDown :up) (isDown :w))
        down (or (isDown :down) (isDown :s))
        dash (or (isDown :m) (isDown :v))
        jump up
        x (+ (if left -1 0) (if right 1 0))
        y (+ (if up -1 0) (if down 1 0))]
    (set sm.down-down down)
    (set sm.x-down (or left right))
    (when (~= sm.state :wall)
      (if left (set sm.direction :left)
          right (set sm.direction :right)))    
    (set sm.angle (math.atan2 y x)) ;; expensive
    (when (and (not jump-down)
               jump
               (< sm.jump-count jump-max)
               (~= sm.state :jump))
      (set jump-down true)
      (set sm.jump-timer jump-time))
    (when (not jump)
      (set jump-down false)
      (set sm.jump-timer 0)
      )
    (when (not dash)      
      (set dash-down false))
    (when (~= sm.state :dash)
      (set sm.dash-timer 0))
    (when (and (not dash-down)
               dash
               (< sm.dash-count dash-max)
               (~= sm.state :dash))
      (set dash-down true)
      (set sm.dash-timer dash-time))
    (local toground (distance-to-nearest-ground-tile player world ground-filter))
    (local towall (distance-to-nearest-wall-tile player world sm.direction ground-filter))
    (when (> 0 towall)
      (set sm.wall-count 0))
    ((. sm sm.state :inputs) toground sm.x-velocity sm.dash-timer sm.jump-timer towall)
    (local (px py) ((. sm sm.state :update) dt player.col.x player.col.y))
    (local (ax ay) (world:move player.col px py (fn [item other]
                                                  (match other.name
                                                      :ground :slide
                                                      _ false))))
    (when (and (~= ax px) (~= sm.state :jump))
      (set sm.x-velocity 0)

      )
    (when (~= ay py)
      (set sm.y-velocity 0))
    (set player.col.x ax)
    (set player.col.y ay)
    (values player ax ay sm.direction sm.state (= 0 toground))))
