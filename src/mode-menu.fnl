(local menu {})

(local gamestate (require :lib.gamestate))
(local fade (require :lib.fade))
(local resources (require :resources))

(local bw 32)
(local bh 32)
(local bx (/ (- 128 bw) 2))
(local by (/ (- 96 bh) 2))
(local cw 12)
(local ch 12)
(local cx (- 128 cw 4))
(local cy (- 96 ch 25))
(local witch {:ox -10 :oy 0 :flip -1})
(var rate 2)
(var hover false)
(var clicked false)

(fn play-hover []
  (resources.click:stop)
  (resources.click:play))

(fn fade-callback [value]
  (fn []
    (resources.beep:stop)
    (resources.beep:play)
    (gamestate.switch (require value)))
  ;; (fade.in)
  )

(fn play-click [value]
  (resources.beep:stop)
  (resources.beep:play)
  (fade.out (fade-callback value)))

(fn menu.enter [self]
  (fade.in))

(fn menu.update [self dt]
  (set witch.oy (+ witch.oy (* rate dt)))
  (if (> witch.oy 5)
      (set rate -2);; use flux
      (< witch.oy 0)
      (set rate 2))
  (var change-hover 0)
  (local (mx my) (love.mouse.getPosition))
  (local (mxp myp) (values (/ mx 6) (/ my 6)))
  (if (and (> mxp bx)
           (> myp by)
           (< mxp (+ bx bw))
           (< myp (+ by bh)))
      (do (when (not hover) (play-hover))
          (set hover :mode-game))
      (set change-hover (+ change-hover 1)))

  (if (and (> mxp cx)
           (> myp cy)
           (< mxp (+ cx cw))
           (< myp (+ cy ch)))
      (do (when (not hover) (play-hover))
          (set hover :mode-credits))
    (set change-hover (+ change-hover 1)))
  (if (= change-hover 2) (set hover false))
  (if (and hover (love.mouse.isDown 1))
      (do  (when (not clicked) (play-click hover))
           (set clicked true))
      (set clicked false))
  (local star1 (. resources.stars :Star1 :animation))
  (local star2 (. resources.stars :Star2 :animation))
  (local star3 (. resources.stars :Star3 :animation))
  (star1:update dt)
  (star2:update dt)
  (star3:update dt))

(fn menu.draw [self]
  (love.graphics.push)
  (love.graphics.scale 6)
  (love.graphics.setColor 1 1 1 1)
  (resources.title-screen:draw :BG)
  (local star-image (. resources.stars :Star1 :image))
  (local star1 (. resources.stars :Star1 :animation))
  (local star2 (. resources.stars :Star2 :animation))
  (local star3 (. resources.stars :Star3 :animation))
  (star1:draw star-image 100 0)
  (star2:draw star-image 66 16)
  (star3:draw star-image 66 16)
  (resources.title-screen:draw :MoonSmall)
  (resources.title-screen:draw :Mountain)
  (love.graphics.draw resources.snow-particles 128 -96)
  (resources.title-screen:draw :Witch witch.ox witch.oy)
  (resources.title-screen:draw :Tiles)
  (resources.title-screen:draw :Text)
  (if (= hover :mode-game)
      (resources.ui:draw :PlayDown bx by)
      (resources.ui:draw :PlayUp bx by))
  (if (= hover :mode-credits)
      (do
        (resources.ui:draw :LightSmall cx cy)
        (love.graphics.setFont resources.textFont)
        (love.graphics.setColor resources.black)
        (love.graphics.print :C (+ cx 6) (+ cy 4))
        (love.graphics.setColor 1 1 1 1)
        )
      (do (resources.ui:draw :DarkSmall cx cy)
          (love.graphics.setFont resources.textFont)
          (love.graphics.setColor resources.black)
          (love.graphics.print :C (+ cx 6) (+ cy 3))
          (love.graphics.setColor 1 1 1 1)))
  (love.graphics.setColor 1 1 1 1)  
  (love.graphics.draw fade.canvas)  
  (love.graphics.pop))

(fn menu.keypressed [self key code]
  (match key
    :escape (gamestate.push (require :mode-dropdown))
    :return (play-click)))


menu
