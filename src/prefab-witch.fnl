(local witch {})
(local world (. (require :state) :col))
(local object (require :object))
(setmetatable witch {:__index object})
(local lg love.graphics)

(local {: clone-animation-table} (require :util))

(fn witch.draw [self image]
  (local {: x : y} self)
  (local s (. self.animations self.state))
  (set s.animation.flippedH self.flipped)
  (lg.setColor 1 1 1 1)  
  (when self.active
    (s.animation:draw s.image x y)))

(fn witch.first [self]
  (object.setup-link self))

(fn witch.link-on [self]  
  (when self.not-spoken
    (set self.col.type :trigger))
  (set self.active true))

(fn witch.link-off [self]
  (set self.col.type :off)
  (set self.active false))

(fn witch.update [self dt]
  (self:first)
  (local s (. self.animations self.state))  
  (when self.active
    (s.animation:update (* 0.2 dt))))

(fn witch.interact [self player]
  (when true
    ;; (love.event.push :witch-spoken-to)
    (set player.state :Idle)
    (local state (require :state))
    (set state.message {:text
                        (if (= 1 self.index)
                            ["Oh hi wanderer welcome to witch mountain!"
                             "As you can see, the mountain has been curesed with endless winter!"
                             "Bring 10 ancient flowers to the summit so I can break the curse!"]
                            (and (= 2 self.index) (< player.flowers 10))
                            [(.. "Oh hi wanderer it looks like you've found " player.flowers " flowers!")
                             "Bring 10 ancient flowers to the summit so I can break the curse!"]
                            (do (and (= 2 self.index))
                                (love.event.push :game-over)
                            [(.. "Oh hi wanderer it looks like you've found " player.flowers " flowers!")
                             "I can finally lift the curse!"]
                            )
                            
                            )
        :i 1 :active true})
    ;; (set self.col.type :off)
    (set self.not-spoken false)))

(local witch-mt {:__index witch})

(fn witch.preview [brush]
  (local {: x : y  : name} brush)
  (local obj
         (setmetatable
          {: name
           :y y
           :x x
           :active true
           :state :Alica
           :animations (clone-animation-table resources.characters)
           :col {:type :witch :name :witch :x (+ x 4) :y (+ y 8) :w 8 :h 8}
           } witch-mt)
         ))

(fn witch.create [brush]
  (local {: x : y  : name : index} brush)
  (local obj
         (setmetatable
          {:brush brush
           : name
           : index
           :state :Alica
           :active false
           :animations (clone-animation-table resources.characters)
           :flipped true
           :w 16
           :h 16
           :y y
           :x x
           :has-run false
           :not-spoken true
           :col {:type :off :name :witch :x x :y y :w 16 :h 16}
           } witch-mt))
  (world:add obj.col obj.col.x obj.col.y obj.col.w obj.col.h)
  obj
  )

witch


