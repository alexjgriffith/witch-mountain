(local box {})

(local lg love.graphics)

(fn box.draw [self]
  (lg.draw self.image self.quad self.pos.x self.pos.y))

(fn box.draw-icon [self]
  (lg.draw self.image self.quad self.spriteName))

(fn box.update [self dt])

(local box-mt {:__index box})

(fn box.new [sprite name x y obj]
  (setmetatable
   {:image (. sprite :IceChest :image)
    :quad (. sprite :IceChest :quad)
    :pos {: x : y}
    :spriteName :IceChest
    :name name} box-mt))

box
