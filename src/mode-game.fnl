(local game {:name :game})
(local lg love.graphics)
(local fade (require :lib.fade))
(local resources (require :resources))
(local state (require :state))
(local gamestate (require :lib.gamestate))

(local target {:x 0 :y -376})
(local camera {:x 0 :y -376 :scale 6 :type :game})

(var world state.col)
(var fps 60)

(fn debug-world []
  (local (items len) (world:getItems))
  (lg.push)  
  (lg.translate (* camera.scale camera.x) (* camera.scale camera.y))
  (lg.setColor 1 1 1 1)
  (for [i 1 len]
    (local {: x : y : w : h} (. items i))
    (love.graphics.rectangle :line
                             (* x camera.scale)
                             (* y camera.scale)
                             (* w camera.scale) (* h camera.scale)))
  (lg.pop))

(fn draw-grid []
  (lg.push)
  (for [i 1 8]
    (for [j 1 8]
      (lg.rectangle :line
                    (* (- 128 16) (- i 1))
                    (- (* 96 (- j 1)) 8)
                    128
                    96
                    )
      )
    )
  (lg.pop))

(tset game :name :game)

(fn game.enter [self]  
  ;;(set level (require :create-level))
  ;; (pp :game-enter)
  (resources.birds:play)  
  (resources.birds:setLooping true)
  (resources.birds:setVolume 0)
  (resources.fire:play)  
  (resources.fire:setLooping true)
  (resources.fire:setVolume 0)  
  (resources.bgm:setVolume 0.1)
  ;;(fade.in (fn [] (resources.level-music:play)))
  (love.mouse.setVisible false)
  (if (not state.level)
      (set state.level (require :create-level)))
  (set world state.col)
  (fade.in)
  )

(fn game.leave [self]
  (love.mouse.setVisible true)
  ;; (resources.bgm:setVolume 0.25)
  (resources.fire:setVolume 0)
  (resources.birds:setVolume 0)
  ;; (resources.bgm:play)
  ;; (resources.level-music:stop)
  )

(fn update-camera [player camera]
  (match camera.type
    :game (do
            (local offset (if (= player.direction :left) -12 -4))
            (local ix (math.floor (/ (+ player.col.x 4 offset) 16)))
            (local iy (math.floor (/ (+ player.col.y 8) 16)))
            (local qx (math.floor (/ ix 7)))
            (local qy (lume.clamp (math.floor (/ iy 6)) 0 4))
            (local tx (- (* qx (- 128 16))))
            (local ty (- (+ -8 (* qy 96))))            
            (when (and (or (~= target.x tx) (~= target.y ty))
                       (< player.col.y 512))
              (player:set-respawn)
              )
            (set target.x tx)
            (set target.y ty)
            (set camera.x  ((if (> tx camera.x) math.ceil math.floor)
                            (lume.lerp target.x camera.x 0.9)))            
            (set camera.y  ((if (> ty camera.y) math.ceil math.floor)
                            (lume.lerp target.y camera.y 0.9))))
    :editor (do (set camera.x 0)
                (set camera.y 0))
  ))

(fn game.update [self dt]
  (set fps (/ 1 dt))
  ;; (+ (* (/ 59 60) fps)
  ;;             (/ 1 60) (/ 1 dt)))
  (local player state.player)
  (update-camera player camera)  
  ;;(: player :update dt)
  (state.level:update-layer :objects dt)
  (state.level:update-layer :bgobjects dt)
  (state.level.editor.update dt camera)
  (local star1 (. resources.stars :Star1 :animation))
  (local star2 (. resources.stars :Star2 :animation))
  (local star3 (. resources.stars :Star3 :animation))
  (star1:update dt)
  (star2:update dt)
  (star3:update dt)
  )

(local green-canvas (lg.newCanvas 600 600))
(local ice-canvas (lg.newCanvas 600 600))
(local obj-temp-canvas (lg.newCanvas 600 600))
(local obj-canvas (lg.newCanvas 600 600))
(local mask-canvas (lg.newCanvas 600 600))
(local invert-mask-canvas (lg.newCanvas 600 600))

(fn draw-mask []
  (local bgobjs (. state.level :layers :bgobjects :data))
  (lg.push)
  (lg.setCanvas mask-canvas)
  (lg.clear)
  (lg.setColor 0 0 0 0)
  (lg.rectangle :fill 0 0 600 600)  
  (love.graphics.setBlendMode :replace)
  (lg.setColor 1 1 1 1)
  (each [key value (ipairs bgobjs)]
    (when (and value.time (> value.time 0) (= value.name :stone))
      (lg.circle :fill (+ 8 value.x) (+ 16 value.y)
                 (* (/ value.time value.period) value.range))))
  (love.graphics.setBlendMode :alpha)
  (lg.setCanvas invert-mask-canvas)
  (lg.clear)
  (lg.setColor 1 1 1 1)
  (lg.rectangle :fill 0 0 600 600)
  (love.graphics.setBlendMode :replace)
  (lg.setColor 0 0 0 0)
  (each [key value (ipairs bgobjs)]
    (when (and value.time (> value.time 0) (= value.name :stone))
      (lg.circle :fill (+ 8 value.x) (+ 16 value.y)
                 (* (/ value.time value.period) value.range))))
  (love.graphics.setBlendMode :alpha)
  (lg.setCanvas)
  (lg.pop))

(fn draw-green [level]  
  (lg.push)  
  (lg.setCanvas green-canvas)
  (lg.clear)
  (lg.setColor 1 1 1 1)
  (lg.draw mask-canvas)
  (love.graphics.setBlendMode :multiply :premultiplied)
  (level:draw-layer :altground)  
  (love.graphics.setBlendMode :alpha :alphamultiply)
  (lg.setCanvas obj-temp-canvas)
  (lg.setColor 1 1 1 0)
  (lg.rectangle :fill 0 0 600 600)
  (lg.setColor 1 1 1 1)
  (level:draw-layer :bgobjects)
  (lg.setCanvas obj-canvas)
  (lg.clear)
  (lg.draw obj-temp-canvas)
  (love.graphics.draw resources.blossom-particles 128 -96)
  (love.graphics.setBlendMode :multiply :premultiplied)  
  (lg.draw mask-canvas)
  ;; 
  (love.graphics.setBlendMode :alpha :alphamultiply)
  (lg.setCanvas)
  (lg.pop))

(fn draw-ice [level]
  (lg.push)
  (lg.setCanvas ice-canvas)
  (lg.setColor 1 1 1 1)
  (lg.clear)  
  (level:draw-layer :ground)
  (level:draw-layer :bgobjects :frozen)  
  (love.graphics.draw resources.snow-particles 128 -96)
  (love.graphics.setBlendMode :multiply :premultiplied)
  (lg.draw invert-mask-canvas)  
  (love.graphics.setBlendMode :alpha :alphamultiply)
  (lg.setCanvas)  
  (lg.pop)
)

(fn love.handlers.game-over []
  (set state.message.callback
       (fn [] (fade.out (fn []
                          (resources.beep:stop)
                          (resources.beep:play)
                          (lume.hotswap :create-level)
                          (gamestate.switch (require :mode-gameover)))))))

(local bg-canvas (love.graphics.newCanvas 128 96))

(local bg-green-canvas (love.graphics.newCanvas 128 96))

(fn draw-ice-background [level]
  (lg.push)
  (lg.setCanvas bg-canvas)
  (resources.title-screen:draw :BG)
  (love.graphics.setBlendMode :multiply :premultiplied)
  (lg.draw invert-mask-canvas
           (math.floor camera.x)
           (math.floor camera.y))
  (love.graphics.setBlendMode :alpha :alphamultiply)
  (lg.setCanvas)
  (lg.pop)
  )


(fn draw-green-background [level]
  (lg.push)
  (lg.setCanvas bg-green-canvas)
  (resources.title-screen:draw "Green Background")
  (love.graphics.setBlendMode :multiply :premultiplied)
  (lg.setColor 1 1 1 0)
  (lg.rectangle :fill
                camera.x
                (- camera.y 8)
                600
                8 
                )
  (lg.rectangle :fill
                (- camera.x 130)
                (- camera.y 8)
                130
                600
                )  
  (lg.setColor 1 1 1 1)
  (lg.draw mask-canvas
           (math.floor camera.x)
           (math.floor camera.y))
  (love.graphics.setBlendMode :alpha :alphamultiply)
  (lg.setCanvas)
  (lg.pop)
  )

(fn game.draw [self]
  (local level state.level)
  (draw-mask)
  (draw-green level)
  (draw-ice level)
  (draw-ice-background level)
  (draw-green-background level)
  (lg.push :all)
  (lg.setCanvas)  
  (lg.scale camera.scale)
  ;; (resources.title-screen:draw :BG)
  (lg.draw bg-canvas)
  (lg.draw bg-green-canvas)
  (local star-image (. resources.stars :Star1 :image))
  (local star1 (. resources.stars :Star1 :animation))
  (local star2 (. resources.stars :Star2 :animation))
  (local star3 (. resources.stars :Star3 :animation))
  (star1:draw star-image 100 0)
  (star2:draw star-image 66 16)
  (star3:draw star-image 66 16)
  (resources.title-screen:draw :MoonSmall
                               0
                               (- (/ camera.y 2) 10))
  (resources.title-screen:draw :Mountain (+ (/ camera.x 10) 20) (+ (/ camera.y 20) 50))
  
  (lg.translate camera.x camera.y)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setBlendMode :alpha :alphamultiply)  
  (lg.draw ice-canvas)
  (lg.draw green-canvas)
  (lg.draw obj-canvas)
  (level:draw-layer :objects)  
  (lg.pop)
  (lg.push)
  (love.graphics.setColor 1 1 1 1)
  (lg.scale camera.scale)
  (love.graphics.draw fade.canvas)
  (lg.pop)
  ;; (lg.print fps 10 10)

  ;; (pp ui)
  (when state.message.active
    (local text-box (. resources :ui :Text))
    (local icon-box (. resources :ui :LightSmall))
    (lg.push)    
    (lg.setColor 1 1 1 1)
    (lg.scale camera.scale)
    (lg.setFont resources.textFont)
    (lg.draw text-box.image text-box.quad 24 16)
    (lg.draw icon-box.image icon-box.quad 89 33)
    (lg.setColor 0 0 0 1)
    (lg.print :s (+ 6 89) (+ 4 33))
    (lg.printf (. state.message.text state.message.i) 26 19 70)
    (lg.setColor 1 1 1 1)
    (lg.pop))
  (lg.setColor 0 0 0 1)
  (local player state.player)
  ;; (lg.print (.. ":x " (math.floor (+ 8 player.x)) ":y " (math.floor (+ 8 player.y))))
  (lg.setColor 1 1 1 1)
  (when state.editor
    (debug-world)
    (draw-grid)
    (level.editor.draw)))

(fn game.mousepressed [self x y button]
  (when state.editor
    (local (t c) (state.level.editor.mousepressed x y button))))

(fn game.mousereleased [self x y button]
  (when state.editor
    (local (t c) (state.level.editor.mousereleased x y button))))

(fn toggle-editor []
  (if state.editor
      (do
        (tset camera :type :game)
        (tset camera :scale 6)
        (set state.editor false))
      (do
        (tset camera :type :editor)
        (tset camera :scale 1)
        (set state.editor true))))

(fn game.keypressed [self key code]
  (match key
    ;; :/ (state.level:save :sample-map.fnl)
    ;; :1 (tset camera :scale 1)
    ;; :2 (tset camera :scale 2)
    ;; :3 (tset camera :scale 3)
    ;; :4 (tset camera :scale 4)
    ;; :5 (tset camera :scale 5)
    ;; :6 (tset camera :scale 6)
    ;; ";" (let [player state.player] (set player.flowers 10)
    ;;          (player:translate 0 0))
    ;; :r (let [player state.player] (player:translate 0 0))
    ;; :delete (toggle-editor)
    :escape (gamestate.push (require :mode-dropdown))
    ;;_ (state.level.editor.keypressed key code)
    ))

game
