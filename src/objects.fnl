(local player (require :prefab-player))
(local house (require :prefab-house))
(local stone (require :prefab-stone))
(local spiker (require :prefab-spiker))
(local flower (require :prefab-flowers))
(local witch (require :prefab-witch))

(local t {})
(fn t.get-create [self]
  (local t [])
  (each [key value (pairs self)]
    (when (= :table (type self))
      (tset t key value.create)))
  t)

(fn t.get-preview [self]
  (local t [])
  (each [key value (pairs self)]
    (when (= :table (type self))
      (tset t key value.preview)))
  t)

(local mt {:__index t})

(setmetatable {: player : house
               : spiker
               :tree house
               :bush house
               :stone stone
               :chest house
               :pot house
               :fence house
               :snow house
               :smalltree house
               :s house
               :v house
               :w house
               :ww house
               :flower flower
               : witch
               } mt)
