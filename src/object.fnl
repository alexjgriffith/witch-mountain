(local object {})

(pp :require-object)

(fn object.remove [self]
  (local world (. (require :state) :col))  
  (fn filter [item]
    (pp [(fennel.view item) (fennel.view self.col)])
    (= (fennel.view item) (fennel.view self.col)))
  (if (world:hasItem self.col)
      (world:remove self.col)
      (let [(items len) (world:queryRect -100 -100 2000 2000 filter)]
        (pp self.col)
        (when (> len 0)
          (world:remove (. items 1))))
      ))

(local lg love.graphics)

(fn object.debug-draw [self camera]
  (local {: x : y : w : h} self)
  (local scale camera.scale)
  (love.graphics.push)
  (lg.translate (* camera.scale camera.x) (* camera.scale camera.y))
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle :line
                           (* scale x)
                           (* scale y)
                           (* scale w)
                           (* scale h))
  (love.graphics.pop))

(fn object.setup-link [self]
  (when (not self.has-run)
    (set self.has-run false)
    (var distance nil)
    (var stone nil)
    (local bgobjs (. (require :state) :level :layers :bgobjects :data))
    (fn dist2 [{:col {:x x1 :y y1}} {:col {:x x2 :y y2}}]
      (+ (^ (- x1 x2) 2) (^ (- y1 y2) 2)))
    (each [_ value (ipairs bgobjs)]
      (when (and value.link (= value.name :stone))
        (local d (dist2 self value))
        (when (or (not distance) (< d distance))
          (set distance d)
          (set stone value))))
    (when stone
      (stone:link self))))

(fn object.serialize [self] self.brush)

object
