(local credits {})
(local gamestate (require :lib.gamestate))
(local fade (require :lib.fade))
(local resources (require :resources))

(local lg love.graphics)


(fn credits.enter [self from]
  (fade.in)
  (set self.from from))

(var click-down false)
(var hovered false)
(fn click [self text]
  (local down (love.mouse.isDown 1))
  (if down
      (when (not click-down)
        (resources.beep:stop)
        (resources.beep:play)
        (match text
          :Back (fade.out (fn [] (gamestate.switch self.from)))
          )
        (set click-down true)
        )
      (do
        (when (~= text hovered)
          (do (set hovered text)
              (resources.click:stop)
              (resources.click:play)))
        (set click-down false)))
  )


(local credits1 "
Game Code - Alexander Griffith
Tiles - GrafxKid
Other Art - Alexander Griffith
Music - TAD
SFX - Little Robot Sound Factory
SFX - Blender Foundation
SFX - MentalSanityOff
Cursor - Kemono
Library (Lume,Flux) - RXI
Engine (LOVE) - LOVE Dev Team
Fennel - Calvin Rose
Web Support - Davidobot")

(fn credits.draw [self]
  (lg.push)
  (lg.clear resources.white)
  (lg.setColor resources.black)
  (lg.scale 6)
  (lg.setFont resources.buttonFont)
  (lg.printf "Credits" 0 2 128 :center)
  (lg.setFont resources.textFont)
  (lg.printf credits1 2 10 128 :left)
  (lg.setColor 1 1 1 1)
  (local (x y) (love.mouse.getPosition))
  (local (mx my) (values (/ x 6) (/ y 6)))
  (local (bw bh) (values (* 48 1) (* 16 1)))
  (local {:quad lsQuad : image} resources.ui.LightSmall)
  (local {:quad dsQuad } resources.ui.DarkSmall)
  (local {:quad xQuad} resources.ui.IconX)
  (let [x 110 y 80]
    (if (pointWithin mx my x y 16 16)
      (do (lg.draw image lsQuad x y)
          (lg.draw image xQuad (+ 4 x) (+ 4 y))
          (click self :Back))
      (do (lg.draw image dsQuad x y)
          (set hovered false)
          (lg.draw image xQuad (+ 4 x) (+ 3 y)))))
  (lg.draw fade.canvas)
  (lg.pop)
  )

(fn credits.update [self])

(fn credits.keypressed [self key code]
  (match key
    :escape (fade.out (fn [] (gamestate.switch self.from)))))

credits
