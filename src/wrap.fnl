;; 1 tiles GrafxKid CC0 https://opengameart.org/content/seasonal-platformer-tiles
;; music (na) https://opengameart.org/content/winter-solstice
;; 2 music (opening) TAD CCBY4.0 https://opengameart.org/content/snowy-cottage-8bit-ver2
;; font https://github.com/MrSimbax/pico-8-font
;; 3 sound effects Little Robot Sound Factory CCBY3.0 https://opengameart.org/content/8-bit-sound-effects-library
;; Kemono CCBY3.0 https://opengameart.org/content/variety-of-cursors
;; MentalSanityOff CCO https://opengameart.org/content/jump-landing-sound
;; Blender Foundation CCBY3.0 https://opengameart.org/content/funny-comic-cartoon-bounce-sound

(local stdio (require :lib.stdio))
(local fade (require :lib.fade))
(local gamestate (require :lib.gamestate))

(fn love.load [args]
  (love.graphics.setDefaultFilter "nearest" "nearest" 0)
  (if (= :web (. args 1)) (global web true) (global web false))
  (global resources (require :resources))
  ;; (love.audio.setVolume 0)
  (fade.set-colour resources.black)
  (love.mouse.setCursor resources.cursor)
  (gamestate.registerEvents)
  (gamestate.switch (require :mode-menu))
  (when (not web) (stdio.start)))

(fn music-update [dt]
  )

(fn love.update [dt]
  (local resources (require :resources))  
  (resources.snow-particles:update dt)
  (resources.blossom-particles:update dt)
  (fade.update (math.min dt 0.032)) ;; don't tie to frame rate
  (music-update dt))

(fn love.keypressed [key]  
  (match key
    :m (if (= 0 (love.audio.getVolume))
           (love.audio.setVolume 1)
           (love.audio.setVolume 0))
    ))
