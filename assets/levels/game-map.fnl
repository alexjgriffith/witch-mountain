{:atlas "/assets/sprites/atlas.png"
 :h 514
 :layers {:altground {:brushes {:altground {:bitmap "bitmap-3x3-simple-alt"
                                            :bitmap-w 4
                                            :char "1"
                                            :ix 0
                                            :iy 4
                                            :name "ground"}
                                :chasm {:bitmap "bitmap-1"
                                        :bitmap-w 1
                                        :char "0"
                                        :ix 0
                                        :iy 11
                                        :name "chasm"}}
                      :encoded-data "00000000000000000000000000000000
00000000000000000000000000000000
00011000000000000000000000001000
00000000011001100000000000001001
00000000110000000000000000001011
11111111100000001111111111001001
10000001100000000000010000001001
10000001000000000000010000001001
10011101000000000000010110001001
10000100000000000000010000001001
10000100000000000000010000001001
11111111111000111100011111001001
10000000000000100000000000001000
10000000000000100000000000001000
10000011110000111111111111111100
10000000000000100000000000000000
10000000000000100000001000010000
11001100001111111110011111111001
10000000000000000000000000001001
10000000000000011100110000001001
10000000000000011000010000001001
10000000000000111000010000001001
10000000000000100000010000001001
11111001111111100000011111001001
10000000001111100100000011001001
11111111111111100111110011001011
10000000111111100000010011001000
10000000011100000000010000001000
10000000000001100000010000001000
11010001110011111111110001111111
00000000000000000000000000000000
00000000000000000000000000000000"
                      :type "tile"}
          :bgobjects {:brushes {:bush {:altsprite "Bush"
                                       :name "bush"
                                       :sprite "IceBush"}
                                :chest {:altsprite "Box"
                                        :name "chest"
                                        :sprite "IceBox"}
                                :fence {:altsprite "Fence"
                                        :name "fence"
                                        :sprite "IceFence"}
                                :house {:altsprite "House"
                                        :name "house"
                                        :sprite "IceHouse"}
                                :pot {:altsprite "Pot"
                                      :name "pot"
                                      :sprite "IcePot"}
                                :s {:altsprite "s" :name "s" :sprite "s"}
                                :smalltree {:altsprite "SmallTree"
                                            :name "smalltree"
                                            :sprite "SmallIceTree"}
                                :snow {:altsprite "Grass"
                                       :name "snow"
                                       :sprite "Snow"}
                                :stone {:altsprite "Hole"
                                        :name "stone"
                                        :sprite "IceHole"}
                                :tree {:altsprite "Tree"
                                       :name "tree"
                                       :sprite "IceTree"}
                                :v {:altsprite "v" :name "v" :sprite "v"}
                                :w {:altsprite "w" :name "w" :sprite "w"}
                                :ww {:altsprite "ww" :name "ww" :sprite "ww"}}
                      :encoded-data [{:altsprite "House"
                                      :name "house"
                                      :sprite "IceHouse"
                                      :x 280
                                      :y 432}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 40
                                      :y 432}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 256
                                      :y 432}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 288
                                      :y 368}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 360
                                      :y 336}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 176
                                      :y 336}
                                     {:altsprite "Bush"
                                      :name "bush"
                                      :sprite "IceBush"
                                      :x 424
                                      :y 448}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 16
                                      :y 240}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 88
                                      :y 192}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 160
                                      :y 240}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 152
                                      :y 144}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 264
                                      :y 144}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 376
                                      :y 96}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 488
                                      :y 16}
                                     {:altsprite "w"
                                      :name "w"
                                      :sprite "w"
                                      :x 24
                                      :y 432}
                                     {:altsprite "ww"
                                      :name "ww"
                                      :sprite "ww"
                                      :x 88
                                      :y 432}
                                     {:altsprite "v"
                                      :name "v"
                                      :sprite "v"
                                      :x 168
                                      :y 448}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 288
                                      :y 384}
                                     {:altsprite "Fence"
                                      :name "fence"
                                      :sprite "IceFence"
                                      :x 112
                                      :y 448}
                                     {:altsprite "SmallTree"
                                      :name "smalltree"
                                      :sprite "SmallIceTree"
                                      :x 32
                                      :y 336}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 32
                                      :y 352}
                                     {:altsprite "Fence"
                                      :name "fence"
                                      :sprite "IceFence"
                                      :x 160
                                      :y 352}
                                     {:altsprite "Fence"
                                      :name "fence"
                                      :sprite "IceFence"
                                      :x 360
                                      :y 352}
                                     {:altsprite "Fence"
                                      :name "fence"
                                      :sprite "IceFence"
                                      :x 400
                                      :y 448}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 240
                                      :y 288}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 160
                                      :y 256}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 128
                                      :y 160}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 240
                                      :y 160}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 368
                                      :y 160}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 88
                                      :y 368}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 400
                                      :y 336}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 256
                                      :y 192}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 368
                                      :y 96}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 16
                                      :y 144}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 412
                                      :y 240}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 152
                                      :y 16}
                                     {:altsprite "Hole"
                                      :name "stone"
                                      :sprite "IceHole"
                                      :x 56
                                      :y 0}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 248
                                      :y 256}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 368
                                      :y 256}
                                     {:altsprite "Fence"
                                      :name "fence"
                                      :sprite "IceFence"
                                      :x 280
                                      :y 208}
                                     {:altsprite "SmallTree"
                                      :name "smalltree"
                                      :sprite "SmallIceTree"
                                      :x 424
                                      :y 192}
                                     {:altsprite "Bush"
                                      :name "bush"
                                      :sprite "IceBush"
                                      :x 360
                                      :y 208}
                                     {:altsprite "Fence"
                                      :name "fence"
                                      :sprite "IceFence"
                                      :x 400
                                      :y 208}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 248
                                      :y 48}
                                     {:altsprite "Grass"
                                      :name "snow"
                                      :sprite "Snow"
                                      :x 312
                                      :y 64}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 8
                                      :y 48}
                                     {:altsprite "Tree"
                                      :name "tree"
                                      :sprite "IceTree"
                                      :x 88
                                      :y 48}
                                     {:altsprite "Pot"
                                      :name "pot"
                                      :sprite "IcePot"
                                      :x 56
                                      :y 64}
                                     {:altsprite "s"
                                      :name "s"
                                      :sprite "s"
                                      :x 256
                                      :y 424}]
                      :type "objects"}
          :ground {:brushes {:chasm {:bitmap "bitmap-1"
                                     :bitmap-w 1
                                     :char "0"
                                     :ix 0
                                     :iy 11
                                     :name "chasm"}
                             :ground {:bitmap "bitmap-3x3-simple-alt"
                                      :bitmap-w 4
                                      :char "1"
                                      :ix 0
                                      :iy 0
                                      :name "ground"}}
                   :encoded-data "00000000000000000000000000000000
00000000000000000000000000000000
00011000000000000000000000001000
00000000011001100000000000001001
00000000110000000000000000001011
11111111100000001111111111001001
10000001100000000000010000001001
10000001000000000000010000001001
10011101000000000000010110001001
10000100000000000000010000001001
10000100000000000000010000001001
11111111111000111100011111001001
10000000000000100000000000001000
10000000000000100000000000001000
10000011110000111111111111111100
10000000000000100000000000000000
10000000000000100000001000010000
11001100001111111110011111111001
10000000000000000000000000001001
10000000000000011100110000001001
10000000000000011000010000001001
10000000000000111000010000001001
10000000000000100000010000001001
11111001111111100000011111001001
10000000001111100100000011001001
11111111111111100111110011001011
10000000111111100000010011001000
10000000011100000000010000001000
10000000000001100000010000001000
11010001110011111111110001111111
00000000000000000000000000000000
00000000000000000000000000000000"
                   :type "tile"}
          :objects {:brushes {:flower {:name "flower"}
                              :player {:name "player"}
                              :spiker {:name "spiker"}
                              :witch {:index 1 :name "witch"}}
                    :encoded-data [{:name "player" :x 8 :y 448}
                                   {:name "spiker" :x 304 :y 440}
                                   {:name "spiker" :x 424 :y 447}
                                   {:name "spiker" :x 56 :y 344}
                                   {:name "spiker" :x 72 :y 255}
                                   {:name "flower" :x 240 :y 448}
                                   {:name "flower" :x 384 :y 352}
                                   {:name "spiker" :x 376 :y 344}
                                   {:name "flower" :x 256 :y 256}
                                   {:name "flower" :x 48 :y 352}
                                   {:name "flower" :x 72 :y 256}
                                   {:index 1 :name "witch" :x 264 :y 448}
                                   {:name "spiker" :x 248 :y 152}
                                   {:name "spiker" :x 56 :y 104}
                                   {:name "spiker" :x 376 :y 152}
                                   {:name "spiker" :x 376 :y 104}
                                   {:name "flower" :x 56 :y 160}
                                   {:name "flower" :x 368 :y 160}
                                   {:name "spiker" :x 212 :y 240}
                                   {:name "spiker" :x 40 :y 152}
                                   {:name "spiker" :x 280 :y 248}
                                   {:name "spiker" :x 392 :y 240}
                                   {:name "flower" :x 468 :y 208}
                                   {:name "flower" :x 216 :y 32}
                                   {:name "spiker" :x 152 :y 24}
                                   {:name "spiker" :x 56 :y 8}
                                   {:name "flower" :x 40 :y 64}
                                   {:index 2 :name "witch" :x 72 :y 64}]
                    :type "objects"}}
 :name "l1"
 :th 32
 :tilesize {:x 16 :y 16}
 :tw 32
 :w 514}