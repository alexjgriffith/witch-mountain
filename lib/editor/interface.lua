local editor = {}
local brush = {}
local cwd = ((...):gsub("%.interface$", "") .. ".")
local level = require((cwd .. "level"))
local pointer_colour = {1, 0, 0, 1}
local snap_ratio = 0.5
brush["brushes"] = {{"ground", "ground", "tile"}, {"ground", "chasm", "tile"}, {"objects", "obj", "object"}}
brush["count"] = 3
brush["index"] = 1
editor.keypressed = function(key, code)
  local _1_ = key
  if (_1_ == "g") then
    return brush.set(1)
  elseif (_1_ == "c") then
    return brush.set(2)
  elseif (_1_ == "o") then
    return brush.set(3)
  elseif (_1_ == "[") then
    return brush.decrement()
  elseif (_1_ == "]") then
    return brush.increment()
  else
    return nil
  end
end
local pointer = {i = 0, j = 0}
local camera = {x = 0, y = 0, scale = 1}
local map_data = nil
local active = false
brush.set = function(index)
  brush.index = index
  return nil
end
brush.decrement = function()
  brush.index = math.max(1, (brush.index - 1))
  return nil
end
brush.increment = function()
  brush.index = math.min(brush.count, (brush.index + 1))
  return nil
end
local function snap(x, y, tilesize, ratio)
  return (math.floor((x / tilesize.x / ratio)) * tilesize.x * ratio), (math.floor((y / tilesize.y / ratio)) * tilesize.y * ratio)
end
local click_down = false
editor.mousereleased = function(x, y, button)
  if active then
    local _local_3_ = {((x / camera.scale) - camera.x), ((y / camera.scale) - camera.y)}
    local cx = _local_3_[1]
    local cy = _local_3_[2]
    local _local_4_ = brush.brushes[brush.index]
    local layer_name = _local_4_[1]
    local brush_name = _local_4_[2]
    local type = _local_4_[3]
    local tilesize = map_data.tilesize
    if ((type == "tile") and (button == 1)) then
      local click_up = {i = math.floor((cx / (tilesize.x * 1))), j = math.floor((cy / (tilesize.y * 1))), button = button}
      local end_y = math.max(click_up.j, click_down.j)
      local start_y = math.min(click_up.j, click_down.j)
      local end_x = math.max(click_up.i, click_down.i)
      local start_x = math.min(click_up.i, click_down.i)
      for i = start_y, end_y do
        for j = start_x, end_x do
          map_data:add(layer_name, (j + 1), (i + 1), brush_name, ((i == end_y) and (j == end_x)))
        end
      end
      click_down = nil
    else
    end
    if ((type == "object") and (button == 1)) then
      local sx, sy = snap(cx, cy, tilesize, snap_ratio)
      map_data:add(layer_name, sx, sy, brush_name)
    else
    end
    if ((type == "object") and (button == 2)) then
      return map_data:remove(layer_name, cx, cy, brush_name)
    else
      return nil
    end
  else
    return nil
  end
end
editor.mousepressed = function(x, y, button)
  if active then
    local _local_9_ = {((x / camera.scale) - camera.x), ((y / camera.scale) - camera.y)}
    local cx = _local_9_[1]
    local cy = _local_9_[2]
    if (button == 1) then
      local tilesize = map_data.tilesize
      click_down = {i = math.floor((cx / (tilesize.x * 1))), j = math.floor((cy / (tilesize.y * 1))), button = button}
      return click_down, camera
    else
      return nil
    end
  else
    return nil
  end
end
local function tile(x, y, tilesize)
  return (math.floor((x / (tilesize.x * 1))) + 1), (math.floor((y / (tilesize.y * 1))) + 1)
end
editor.draw = function()
  if active then
    local x, y = love.mouse.getPosition()
    local _local_12_ = {((x / camera.scale) - camera.x), ((y / camera.scale) - camera.y)}
    local cx = _local_12_[1]
    local cy = _local_12_[2]
    local tilesize = map_data.tilesize
    local _local_13_ = brush.brushes[brush.index]
    local layer_name = _local_13_[1]
    local brush_name = _local_13_[2]
    local type = _local_13_[3]
    local sx, sy = snap(cx, cy, tilesize, snap_ratio)
    local tx, ty = tile(cx, cy, tilesize)
    love.graphics.push()
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.scale(camera.scale)
    love.graphics.translate(camera.x, camera.y)
    do
      local _14_ = type
      if (_14_ == "object") then
        map_data:preview(layer_name, sx, sy, brush_name, type)
      elseif (_14_ == "tile") then
        map_data:preview(layer_name, tx, ty, brush_name, type)
      else
      end
    end
    love.graphics.pop()
    love.graphics.push()
    local tilesize0 = map_data.tilesize
    love.graphics.setColor(pointer_colour)
    love.graphics.rectangle("line", (((tilesize0.x * pointer.i) + camera.x) * camera.scale), (((tilesize0.y * pointer.j) + camera.y) * camera.scale), (camera.scale * tilesize0.x), (camera.scale * tilesize0.y), 20)
    love.graphics.pop()
    return love.graphics.setColor(1, 1, 1, 1)
  else
    return nil
  end
end
editor["update-camera"] = function(_17_)
  local _arg_18_ = _17_
  local x = _arg_18_["x"]
  local y = _arg_18_["y"]
  local scale = _arg_18_["scale"]
  camera.x = (x or 0)
  camera.y = (y or 0)
  camera.scale = (scale or 1)
  return camera
end
editor["update-pointer"] = function()
  local ix, iy = love.mouse.getPosition()
  local offsets = {gx = 0, s = 1, gy = 0}
  local _local_19_ = {((ix - offsets.gx) / offsets.s), ((iy - offsets.gy) / offsets.s)}
  local x = _local_19_[1]
  local y = _local_19_[2]
  local tilesize = map_data.tilesize
  pointer["i"] = math.floor((((x / camera.scale) - camera.x) / tilesize.x))
  do end (pointer)["j"] = math.floor((((y / camera.scale) - camera.y) / tilesize.y))
  return nil
end
editor["set-level"] = function(new_level)
  map_data = new_level
  return nil
end
editor.active = function()
  return active
end
editor.toggle = function()
  active = not active
  return nil
end
editor.update = function(dt, camera_3f)
  if active then
    if camera_3f then
      editor["update-camera"](camera_3f)
    else
    end
    return editor["update-pointer"]()
  else
    return nil
  end
end
editor.init = function(new_map, new_camera, new_pointer_colour_3f)
  active = true
  map_data = new_map
  if new_pointer_colour_3f then
    pointer_colour = new_pointer_colour_3f
  else
  end
  return editor["update-camera"](camera, new_camera)
end
return {editor = editor, brush = brush}
