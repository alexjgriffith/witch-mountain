(local map {})
;; Wraper around autotile for multiple layers and objects
;; Needs to be cleanly serilizable

(local cwd  (.. (: ... :gsub "%.map$" "")  "."))

(local autotile (require (.. cwd :lib.autotile)))

(fn xy-to-id [x y width]
    (+ (* x width) y))

(macro append [s v]
  `(do (set ,s (.. ,s ,v)) ,s))

(fn zeros [x y]
  (var ret "")
  (for [i 1 y]
    (for [j 1 x]
      (append ret "0"))
    (append ret "\n"))
  ret)

(fn map.empty [name w h tilesize atlas]
  (local tw (math.floor (/ w tilesize.x)))
  (local th (math.floor (/ h tilesize.y)))
  {:name name
   :w w
   :h h
   :tw tw
   :th th
   :atlas atlas
   :tilesize tilesize
   :layers {}})

(fn map.add-layer [map-data layer type brushes atlas? encoded-data?]
  (local encoded-data
         (match (values type encoded-data?)
           (:tile nil) (zeros map-data.tw map-data.th)
           (:objects nil) {}
           (_ _) encoded-data?))
  (local l {: type
            : brushes
            :atlas atlas?
            : encoded-data})
  (tset map-data.layers layer l)
  (values map-data l))


(fn map.add-brush [map-data layer-name brush-name brush]
  (tset (. map-data :layers layer-name :brushes) brush-name brush)
  (values map-data brush))

(fn map.set-layer-image [map-data layer-name image-filename]
  (tset (. map-data :layers layer-name) :image image-filename)
  map-data)

(fn map.set-name [map-data filename]
  (tset map-data :name filename)
  (values map-data filename))

(fn clone [obj]
  (let [ret {}]
    (each [key value (pairs obj)]
      (tset ret key value))
    ret))

(fn map.add-object [map-data layer-name x y brush-name
                    object-generation-callbacks ...]
  (local layer (. map-data :layers layer-name))
  (local brush (clone (. layer :brushes brush-name)))
  (tset brush :x x)
  (tset brush :y y)
  (local obj ((. object-generation-callbacks brush.name) brush ...))
  (when (not layer.data) (tset layer :data []))
  (table.insert layer.data obj)
  map-data)

(fn map.add-object-encoded [map-data layer-name x y brush-name
                    object-generation-callbacks ...]
  (local layer (. map-data :layers layer-name))
  (local brush (clone (. layer :brushes brush-name)))
  (tset brush :x x)
  (tset brush :y y)
  (local obj ((. object-generation-callbacks brush.name) brush ...))  
  (when (not layer.encoded-data) (tset layer :encoded-data []))
  (table.insert layer.encoded-data (obj:serialize))
  map-data)

(fn map.remove-object [map-data layer-name x y ]
  (let [rem []
        data (. map-data :layers layer-name :data)]
    (each [key {:x xd :y yd :w wd :h hd} (ipairs data)]
      (when (and (> x xd) (> y yd) (< x (+ xd hd)) (< y (+ yd hd)))
        (table.insert rem key)))
    (if (> (# rem) 0)
      (let [i (?. rem (# rem))
            obj (. data i)
            brush (obj:serialize)]      
        (table.remove data i)
        (values map-data brush obj))
      (values map-data))))

(fn serialize-objects [data]
  (let [encoded-data []]    
    (each [key obj (ipairs data)]
      (table.insert encoded-data (obj:serialize)))
    encoded-data))

(fn deserialize-objects [encoded-data image callbacks]
  (let [objs []]
    (each [key obj (ipairs encoded-data)]
      ;; (db :deserialize-objects obj)
      (table.insert objs ((. callbacks obj.name) obj image)))
    objs))

(fn make-autotile-params [brushes]
  (let [params []]
    (each [_ brush (pairs brushes)]
      (table.insert params [brush.char
                            (. autotile brush.bitmap)
                            brush.bitmap-w]))
    params))

(fn map.deserialize [map-data object-generation-callbacks]
  (each [layer-name layer (pairs map-data.layers)]
    (match layer.type
      :tile (let [brush-map (let [ret []] (each [_ b (pairs layer.brushes)]
                                         (tset ret b.char b.name))
                                 ret)
                  decoded-tiles
                  (autotile.decode layer.encoded-data
                                   map-data.tw
                                   (make-autotile-params layer.brushes))]
              (autotile.set-brushes decoded-tiles brush-map)
              ;; (db :autotile-params (make-autotile-params layer.brushes))
              ;; (db :decoded-tiles decoded-tiles)
              (tset layer :data decoded-tiles))
      :objects (tset layer :data (deserialize-objects
                                 layer.encoded-data
                                 layer.image
                                 object-generation-callbacks))
      ))
  map-data)

(fn map.clear-encoded-data [map-data]
  (each [_layer-name layer (pairs map-data.layers)]
    (tset layer :encoded-data nil))
  map-data)

(fn clear-data [map-data]
  (each [_layer-name layer (pairs map-data.layers)]
    (tset layer :data nil))
  map-data)

(fn clone-encoded-layer [layer]
  (let [{: type
         : brushes         
         : encoded-data} layer
        atlas? layer.atlas]
    {: type : brushes : encoded-data :atlas atlas?}))

(fn clone-encoded-data [map-data]  
  (let [{: name : w : h : tw : th : atlas : tilesize} map-data]
    (local save-map {: name : w : h : tw : th : atlas : tilesize
                     :layers {}})
    (each [key layer (pairs map-data.layers)]
      (tset save-map.layers key (clone-encoded-layer layer)))
    save-map))

(fn map.serialize [map-data]
    (each [layer-name layer (pairs map-data.layers)]
      (match layer.type
        :tile (tset layer
                    :encoded-data (autotile.encode layer.data))
        :objects (tset layer
                      :encoded-data (serialize-objects layer.data))))
    (clone-encoded-data map-data))

(fn map.replace-tile [map-data layer-name x y brush-name reautotile?]
  (local layer (. map-data :layers layer-name))
  (local brush (clone (. layer :brushes brush-name)))
  (autotile.set-tile layer.data x y map-data.tw brush.char brush-name)
  (when reautotile?
    (tset layer :data (autotile.re-autotile layer.data map-data.tw brush.char
                        (. autotile brush.bitmap) brush.bitmap-w)))
  map-data)

(fn map.y-sort-layer [map-data layer-name]
  (local layer (map.get-layer map-data layer-name))
  (when (= :objects layer.type)
    (table.sort layer.data (fn [a b]
                             (if (< a.y b.y) true
                                 (= a.y b.y) (< a.x b.x)
                                 false
                                 )
                             )))
  map-data)

(fn map.get-layer [map-data layer-name]
  (. map-data :layers layer-name))

(fn map.get-brush [map-data layer-name brush-name]
  (. map-data :layers layer-name :brushes brush-name))

(fn map.get-layers [map-data]
  (local ret [])
  (each [layer-name _ (pairs map-data.layers)]
    (table.insert ret layer-name))
  ret)

(fn map.get-brushes [map-data layer-name]
  (local brushes (. map-data :layers layer-name :brushes))
  brushes)

(fn map.quad-regions [map-data layer-name iw ih x? y?]
  (local t [])
  (local brushes (. map-data :layers layer-name :brushes))
  (local tilesize map-data.tilesize)  
  (each [brush-name brush (pairs brushes)]
    (tset t brush.char (autotile.quad-regions
                        (autotile.parse-bitmap (. autotile brush.bitmap)
                                               brush.bitmap-w)
                                              brush.bitmap-w
                                              (+ (* tilesize.x brush.ix)
                                                 (or x? 0))
                                              (+ (* tilesize.y brush.iy)
                                                 (or y? 0))
                                              tilesize.x
                                              tilesize.y iw ih)))
  t)

map
