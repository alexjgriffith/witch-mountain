local level = {}
local cwd = ((...):gsub("%.level$", "") .. ".")
local map = require((cwd .. "map"))
level["load-map"] = function(map_file, callbacks, previews)
  local map_data
  if (love.filesystem.isFused() and love.filesystem.exists((map_file .. ".fnl"))) then
    pp(("Game is Fused: Loading from " .. map_file .. ".fnl"))
    map_data = fennel.eval(love.filesystem.read((map_file .. ".fnl")), {})
  else
    map_data = require(map_file)
  end
  return level.initialize(map.deserialize(map_data, callbacks), callbacks, previews)
end
love.handlers["edit-level"] = function(key, ...)
  return pp({key, ...})
end
level.add = function(map_data, layer_name, x, y, brush_name, reautotile_3f)
  local _let_2_ = map_data
  local object_creation_callbacks = _let_2_["object-creation-callbacks"]
  local layer = map["get-layer"](map_data, layer_name)
  local brush = map["get-brush"](map_data, layer_name, brush_name)
  if ("tile" == layer.type) then
    map["replace-tile"](map_data, layer_name, x, y, brush_name, reautotile_3f)
    if reautotile_3f then
      level["update-spritebatch"](map_data, layer_name)
    else
    end
    love.event.push("edit-level", "replace-tile", layer_name, brush_name, x, y, map_data.tilesize)
    return map_data, brush
  else
    map["add-object"](map_data, layer_name, x, y, brush_name, object_creation_callbacks)
    love.event.push("edit-level", "add-object", layer_name, brush_name, x, y)
    return map_data, brush
  end
end
level.remove = function(map_data, layer_name, x, y)
  local _, brush, obj = map["remove-object"](map_data, layer_name, x, y)
  if obj then
    love.event.push("edit-level", "remove-object", layer_name, brush.name, obj)
  else
  end
  return map_data, brush
end
level.save = function(map_data, file)
  do
    local f = assert(io.open(file, "wb"))
    local c = f:write(fennel.view(map.serialize(map_data)))
    f:close()
  end
  return nil
end
local slow = false
local function draw_tile_layer(image, quads, data, layer_name, tilesize, spritebatch)
  love.graphics.push()
  if slow then
    for _index, _6_ in ipairs(data) do
      local _each_7_ = _6_
      local x = _each_7_["x"]
      local y = _each_7_["y"]
      local bitmap_index = _each_7_["bitmap-index"]
      local ignore = _each_7_["ignore"]
      local char = _each_7_["char"]
      if (bitmap_index ~= ignore) then
        love.graphics.draw(image, quads[char][bitmap_index], ((x - 1) * tilesize.x), ((y - 1) * tilesize.y))
      else
      end
    end
  else
    love.graphics.draw(spritebatch)
  end
  return love.graphics.pop()
end
local function draw_object_layer(image, objects, ...)
  love.graphics.push()
  for _index, _10_ in ipairs(objects) do
    local _each_11_ = _10_
    local x = _each_11_["x"]
    local y = _each_11_["y"]
    local object = _each_11_
    if object.draw then
      object:draw(image, ...)
    else
    end
  end
  return love.graphics.pop()
end
level["draw-layer"] = function(map_data, layer_name, ...)
  local _local_13_ = map_data.layers[layer_name]
  local quads = _local_13_["quads"]
  local image = _local_13_["image"]
  local data = _local_13_["data"]
  local type = _local_13_["type"]
  local spritebatch = _local_13_["spritebatch"]
  local tilesize = map_data.tilesize
  do
    local _14_ = type
    if (_14_ == "tile") then
      draw_tile_layer(image, quads, data, layer_name, tilesize, spritebatch)
    elseif (_14_ == "objects") then
      draw_object_layer((image or map_data.image), data, ...)
    else
    end
  end
  return map_data
end
local function clone(obj)
  local ret = {}
  for key, value in pairs(obj) do
    ret[key] = value
  end
  return ret
end
level.preview = function(map_data, layer_name, x, y, brush_name, type)
  local layer = map["get-layer"](map_data, layer_name)
  local brush = clone(map["get-brush"](map_data, layer_name, brush_name))
  do end (brush)["x"] = x
  brush["y"] = y
  local _16_ = type
  if (_16_ == "object") then
    local obj = (map_data["object-preview-callbacks"])[brush.name](brush)
    return draw_object_layer((layer.image or map_data.image), {obj})
  elseif (_16_ == "tile") then
    local _let_17_ = brush
    local char = _let_17_["char"]
    local _let_18_ = map_data
    local tilesize = _let_18_["tilesize"]
    local image = (layer.image or map_data.image)
    local quads = layer.quads
    return love.graphics.draw(image, quads[char][1], ((x - 1) * tilesize.x), ((y - 1) * tilesize.y))
  else
    return nil
  end
end
local function update_object_layer(dt, data, ...)
  for _, obj in ipairs(data) do
    if obj.update then
      obj:update(dt, ...)
    else
    end
  end
  return nil
end
level["update-layer"] = function(map_data, layer_name, dt, ...)
  local _local_21_ = map_data.layers[layer_name]
  local data = _local_21_["data"]
  local type = _local_21_["type"]
  local _22_ = type
  if (_22_ == "tile") then
    return "nil"
  elseif (_22_ == "objects") then
    return update_object_layer(dt, data, ...)
  else
    return nil
  end
end
local map_mt = {__index = map}
setmetatable(level, map_mt)
local level_mt = {__index = level}
local function quad_regions_to_quads(quad_regions)
  local ret = {}
  for name, regions in pairs(quad_regions) do
    ret[name] = {}
    for key, _24_ in pairs(regions) do
      local _each_25_ = _24_
      local x = _each_25_["x"]
      local y = _each_25_["y"]
      local w = _each_25_["w"]
      local h = _each_25_["h"]
      local iw = _each_25_["iw"]
      local ih = _each_25_["ih"]
      ret[name][key] = love.graphics.newQuad(x, y, w, h, iw, ih)
    end
  end
  return ret
end
level["set-quads"] = function(map_data, layer_name, ...)
  local layer = map_data.layers[layer_name]
  local _let_26_ = layer
  local type = _let_26_["type"]
  local data = _let_26_["data"]
  layer["quads"] = {}
  do
    local _27_ = type
    if (_27_ == "tile") then
      local image = (layer.image or map_data.image)
      local iw, ih = image:getDimensions()
      do end (layer)["quads"] = quad_regions_to_quads(map["quad-regions"](map_data, layer_name, iw, ih, ...))
    elseif (_27_ == "objects") then
    else
    end
  end
  return map_data
end
level["update-spritebatch"] = function(map_data, layer_name)
  local _let_29_ = map["get-layer"](map_data, layer_name)
  local spritebatch = _let_29_["spritebatch"]
  local quads = _let_29_["quads"]
  local data = _let_29_["data"]
  local layer = _let_29_
  spritebatch:clear()
  local tilesize = map_data.tilesize
  for _, _30_ in ipairs(data) do
    local _each_31_ = _30_
    local char = _each_31_["char"]
    local x = _each_31_["x"]
    local y = _each_31_["y"]
    local bitmap_index = _each_31_["bitmap-index"]
    local d = _each_31_
    spritebatch:add(quads[char][bitmap_index], ((x - 1) * tilesize.x), ((y - 1) * tilesize.y))
  end
  return nil
end
level["add-layer"] = function(map_data, layer_name, ...)
  map.deserialize(map["add-layer"](map_data, layer_name, ...), map_data["object-creation-callbacks"])
  local layer = map_data.layers[layer_name]
  if layer.atlas then
    layer["image"] = love.graphics.newImage(layer.atlas)
  else
  end
  if layer.data then
    level["set-quads"](map_data, layer_name)
  else
  end
  if ("tile" == layer.type) then
    level["add-spritebatch"](map_data, layer_name)
  else
  end
  return map_data, layer
end
level["add-spritebatch"] = function(map_data, layer_name)
  local layer = map["get-layer"](map_data, layer_name)
  local _local_35_ = map_data
  local tw = _local_35_["tw"]
  local th = _local_35_["th"]
  local image = _local_35_["image"]
  local sb = love.graphics.newSpriteBatch((layer.image or image), (tw * th))
  do end (layer)["spritebatch"] = sb
  level["update-spritebatch"](map_data, layer_name)
  return map_data
end
level.initialize = function(map_data, callbacks, previews)
  local _local_36_ = map_data
  local name = _local_36_["name"]
  local w = _local_36_["w"]
  local h = _local_36_["h"]
  local tilesize = _local_36_["tilesize"]
  local atlas = _local_36_["atlas"]
  map_data["object-creation-callbacks"] = callbacks
  map_data["object-preview-callbacks"] = previews
  local tw, th = (w / tilesize.x), (h / tilesize.y)
  do end (map_data)["image"] = love.graphics.newImage(atlas)
  for layer_name, layer in pairs(map_data.layers) do
    if layer.atlas then
      layer["image"] = love.graphics.newImage(layer.atlas)
    else
    end
    if layer.data then
      level["set-quads"](map_data, layer_name)
    else
    end
    local _39_ = layer.type
    if (_39_ == "tile") then
      level["add-spritebatch"](map_data, layer_name)
      for _, d in ipairs(layer.data) do
        love.event.push("edit-level", "replace-tile", layer_name, d.brush, d.x, d.y, map_data.tilesize)
      end
    elseif (_39_ == "objects") then
    else
    end
  end
  return setmetatable(map_data, level_mt)
end
level.create = function(name, w, h, tilesize, atlas, callbacks, previews)
  local tilesize_table
  if ("table" == type(tilesize)) then
    tilesize_table = tilesize
  else
    tilesize_table = {x = tilesize, y = tilesize}
  end
  local map_data = map.deserialize(map.empty(name, w, h, tilesize_table, atlas), callbacks)
  return level.initialize(map_data, callbacks, previews)
end
return level
