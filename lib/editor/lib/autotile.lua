local autotile = {}
autotile["set-index"] = function(self, i, w, ...)
  local xi = (1 + ((i - 1) % w))
  local yi = (1 + math.floor(((i - 1) / w)))
  local t = {x = xi, y = yi}
  local args = {...}
  local l = #args
  for i0 = 1, l, 2 do
    t[args[i0]] = args[(i0 + 1)]
  end
  self[i] = t
  return self
end
autotile.set = function(self, x, y, w, ...)
  local i = (1 + (w * (y - 1)) + (x - 1))
  local t = {x = x, y = y}
  local args = {...}
  local l = #args
  for i0 = 1, l, 2 do
    t[args[i0]] = args[(i0 + 1)]
  end
  self[i] = t
  return self
end
autotile["set-tile"] = function(self, x, y, w, char, brush, _3fbitmap_index, _3fignore)
  return autotile.set(self, x, y, w, "bitmap-index", (_3fbitmap_index or "NA"), "char", char, "brush", brush, "ignore", (_3fignore or "NA"))
end
autotile["bitmap-3x3-simple"] = "\n000 000 000 000\n010 011 111 110\n010 01_ _1_ _10\n\n010 01_ _1_ _10\n010 011 111 110\n010 01_ _1_ _10\n\n010 01_ _1_ _10\n010 011 111 110\n000 000 000 000\n\n000 000 000 000\n010 011 111 110\n000 000 000 000\n"
autotile["bitmap-3x3-simple-alt"] = "\n_0_ _0_ _0_ _0_\n010 011 111 110\n_1_ _1_ _1_ _1_\n\n_1_ _1_ _1_ _1_\n010 011 111 110\n_1_ _1_ _1_ _1_\n\n_1_ _1_ _1_ _1_\n010 011 111 110\n_0_ _0_ _0_ _0_\n\n_0_ _0_ _0_ _0_\n010 011 111 110\n_0_ _0_ _0_ _0_\n"
autotile["bitmap-2x2"] = "\n_0_ _11 110 _0_\n110 011 111 111\n11_ _11 111 111\n\n110 011 1_1 111\n111 111 _1_ 111\n011 111 1_1 110\n\n_11 111 111 11_\n011 111 111 110\n_0_ _0_ 011 11_\n\n000 _0_ 011 11_\n010 011 111 110\n000 _11 110 _0_\n"
autotile["bitmap-1"] = "\n___\n_1_\n___\n"
local function n(t, i, w, d)
  local function check(t0, i0, oy, ox, d0)
    local row = math.floor(((i0 + oy + -1) / w))
    local index = (i0 + oy + ox)
    local index_row = math.floor(((index - 1) / w))
    if ((index_row == row) and (t0)[index]) then
      return (t0)[index]
    else
      return d0
    end
  end
  return {check(t, i, ( - w), -1, d), check(t, i, ( - w), 0, d), check(t, i, ( - w), 1, d), check(t, i, 0, 1, d), check(t, i, w, 1, d), check(t, i, w, 0, d), check(t, i, w, -1, d), check(t, i, 0, -1, d)}
end
local function f_n(t, i, w, d)
  local r = {t[i]}
  local t2 = n(t, i, w, d)
  for k, v in ipairs(t2) do
    table.insert(r, v)
  end
  return r
end
autotile["parse-bitmap"] = function(text, w)
  local l = #text
  local t = {}
  local t2 = {}
  local t3 = {}
  for i = 1, l do
    local _2_ = string.sub(text, i, (i + 0))
    if (_2_ == "\n") then
    elseif (_2_ == " ") then
    elseif (nil ~= _2_) then
      local tile = _2_
      table.insert(t, tile)
    else
    end
  end
  for i = 1, #t do
    if ((1 == (math.floor((i / (3 * w))) % 3)) and (2 == (math.floor((i % (3 * w))) % 3))) then
      table.insert(t2, n(t, i, (3 * w), "_"))
    else
    end
  end
  for i, list in ipairs(t2) do
    local r = {0}
    local function increment_r(k)
      for i0, v in ipairs(r) do
        r[i0] = (v + (2 ^ k))
      end
      return nil
    end
    local function expand_r(k)
      local l0 = #r
      for i0 = 1, l0 do
        local v = r[i0]
        r[i0] = (v + (2 ^ k))
        table.insert(r, v)
      end
      return nil
    end
    for k, l0 in ipairs(list) do
      local _5_ = l0
      if (_5_ == "0") then
      elseif (_5_ == "1") then
        increment_r((k - 1))
      elseif (_5_ == "_") then
        expand_r((k - 1))
      else
      end
    end
    for _, v in ipairs(r) do
      t3[v] = i
    end
  end
  return t3
end
autotile.parse = function(text, w)
  local l = #text
  local t = {}
  for i = 1, l do
    local _7_ = string.sub(text, i, (i + 0))
    if (_7_ == "\n") then
    elseif (_7_ == " ") then
    elseif (nil ~= _7_) then
      local tile = _7_
      table.insert(t, tile)
    else
    end
  end
  return t
end
autotile.neighbours = function(map, w)
  local t = {}
  for i, value in ipairs(map) do
    table.insert(t, f_n(map, i, w, "_"))
  end
  return t
end
autotile["match-key"] = function(map, w, char, __3f)
  local t = {}
  for _, row in ipairs(map) do
    local j = 0
    local v = row[1]
    for i, value in ipairs(row) do
      if (i > 1) then
        local _9_ = value
        if (_9_ == "_") then
          if __3f then
            j = (j + (2 ^ (i - 2)))
          else
          end
        elseif (_9_ == char) then
          j = (j + (2 ^ (i - 2)))
        elseif true then
          local _0 = _9_
        else
        end
      else
      end
    end
    if (v ~= char) then
      j = "_"
    else
    end
    table.insert(t, j)
  end
  return t
end
autotile["match-bitmap"] = function(map, bitmap, name)
  local t = {}
  for _, v in ipairs(map) do
    local o = bitmap[v]
    table.insert(t, (o or "NA"))
  end
  return t
end
autotile["to-sparse"] = function(map, w, char, ignore)
  local s = {}
  for i, index in ipairs(map) do
    autotile["set-index"](s, i, w, "bitmap-index", index, "char", char, "ignore", ignore)
  end
  return s
end
autotile["set-brushes"] = function(map, brush_map)
  for i, sparse in ipairs(map) do
    sparse["brush"] = brush_map[sparse.char]
  end
  return map
end
autotile.merge = function(map, ...)
  for _, m in ipairs({...}) do
    for k, v in ipairs(m) do
      if ("NA" == map[k]["bitmap-index"]) then
        map[k] = v
      else
      end
    end
  end
  return map
end
autotile["quad-regions"] = function(bitmap, w, px, py, pw, ph, iw, ih)
  local quad_regions = {}
  for _, key in pairs(bitmap) do
    quad_regions[key] = {x = (px + (pw * math.floor(((key - 1) % w)))), y = (py + (ph * math.floor(((key - 1) / w)))), w = pw, h = ph, iw = iw, ih = ih}
  end
  return quad_regions
end
autotile["re-autotile"] = function(sparse_map, map_w, char, bitmap, bitmap_w)
  local t = {}
  for _, _15_ in ipairs(sparse_map) do
    local _each_16_ = _15_
    local value = _each_16_["char"]
    table.insert(t, value)
  end
  return autotile.merge(autotile["to-sparse"](autotile["match-bitmap"](autotile["match-key"](autotile.neighbours(t, map_w), map_w, char, true), autotile["parse-bitmap"](bitmap, bitmap_w)), map_w, char, "NA"), sparse_map)
end
autotile.decode = function(str, w, params)
  local t = false
  for _, _17_ in ipairs(params) do
    local _each_18_ = _17_
    local char = _each_18_[1]
    local bitmap = _each_18_[2]
    local bitmap_w = _each_18_[3]
    local d = autotile["to-sparse"](autotile["match-bitmap"](autotile["match-key"](autotile.neighbours(autotile.parse(str, w), w), w, char, true), autotile["parse-bitmap"](bitmap, bitmap_w)), w, char, "NA")
    if t then
      autotile.merge(t, d)
    else
      t = d
    end
  end
  return t
end
autotile.encode = function(sparse_map)
  local row = 1
  local ret = ""
  for key, value in ipairs(sparse_map) do
    if (value.y > row) then
      row = value.y
      ret = (ret .. "\n")
    else
    end
    ret = (ret .. value.char)
  end
  return ret
end
return autotile
