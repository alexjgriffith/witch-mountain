(local level {})
;; Wrapper around map integrating love calls

(local cwd  (.. (: ... :gsub "%.level$" "")  "."))
(local map (require (.. cwd :map)))

(fn level.load-map [map-file callbacks previews]
  (let [map-data (if (and (love.filesystem.isFused)
                          (love.filesystem.exists (.. map-file ".fnl")))
                     (do (pp (.. "Game is Fused: Loading from " map-file ".fnl"))
                         (fennel.eval (love.filesystem.read (.. map-file ".fnl")) {}))                     
                     (require map-file))]
    (-> map-data
        (map.deserialize callbacks)        
        (level.initialize callbacks previews))))

(fn love.handlers.edit-level [key ...]
  (pp [key ...] ))

(fn level.add [map-data layer-name x y brush-name reautotile?]
  (let [{: object-creation-callbacks} map-data
        layer (map.get-layer map-data layer-name)
        brush (map.get-brush map-data layer-name brush-name)]
    (if (= :tile layer.type)
        (do
          (map.replace-tile map-data layer-name x y brush-name reautotile?)
          (when reautotile?
            (level.update-spritebatch  map-data layer-name))
          ;; (level.set-sprite-batch self layer-name)
          (love.event.push :edit-level :replace-tile layer-name brush-name
                           x y
                           map-data.tilesize)
          (values map-data brush))
        (do
          (map.add-object map-data layer-name x y brush-name object-creation-callbacks)
          (love.event.push :edit-level :add-object layer-name brush-name x y)
          (values map-data brush)))))

(fn level.remove [map-data layer-name x y]
  (local (_ brush obj) (map.remove-object map-data layer-name x y))
  (when obj
    (love.event.push :edit-level :remove-object layer-name brush.name obj))
  (values map-data brush))

(fn level.save [map-data file]
  (let [f (assert (io.open file "wb"))
        c (: f :write  (fennel.view (map.serialize map-data)))]
    (: f :close)
    c)    
  nil)

(var slow false)
(fn draw-tile-layer [image quads data layer-name tilesize spritebatch]
  (love.graphics.push)
  (if slow
   (each [_index {: x : y : bitmap-index : ignore : char} (ipairs data)]    
     (when (~= bitmap-index ignore)
       (love.graphics.draw image (. quads char bitmap-index)
                           (* (- x 1) tilesize.x) (* (- y 1) tilesize.y))))
   (love.graphics.draw spritebatch))
  (love.graphics.pop))

(fn draw-object-layer [image objects ...]
  (love.graphics.push)
  (each [_index {: x : y &as object} (ipairs objects)]
    (when object.draw
      (object:draw image ...)))
  (love.graphics.pop))

(fn level.draw-layer [map-data layer-name ...]
  (local {: quads : image : data : type : spritebatch}
         (. map-data :layers layer-name))
  (local tilesize map-data.tilesize)
  (match  type
    :tile (draw-tile-layer image quads data layer-name tilesize spritebatch)
    :objects (draw-object-layer (or image map-data.image) data ...))
  map-data)

(fn clone [obj]
  (let [ret {}]
    (each [key value (pairs obj)]
      (tset ret key value))
    ret))

(fn level.preview [map-data layer-name x y brush-name type]
  (local layer (map.get-layer map-data layer-name))
  (local brush (clone (map.get-brush map-data layer-name brush-name)))
  (tset brush :x x)
  (tset brush :y y)
  (match type
    :object (let [obj ((. map-data.object-preview-callbacks brush.name) brush)]
              (draw-object-layer (or layer.image map-data.image) [obj])
              ;; (obj:remove)
              )
    :tile (let [{: char } brush
                {: tilesize} map-data
                image (or layer.image map-data.image)
                quads (. layer :quads)]
            (love.graphics.draw image (. quads char 1)
                                (* (- x 1) tilesize.x) (* (- y 1) tilesize.y)))))

(fn update-object-layer [dt data ...]
  (each [_ obj (ipairs data)]
    (when obj.update
      (obj:update dt ...))
    ))

(fn level.update-layer [map-data layer-name dt ...]
  (local {: data : type} (. map-data :layers layer-name))
  (match  type
    :tile :nil    
    :objects (update-object-layer dt data ...)
    ))

;; real hacky inheritance
(local map-mt {:__index map})
(setmetatable level map-mt)
(local level-mt {:__index level})

(fn quad-regions-to-quads [quad-regions]
  (local ret [])
  (each [name regions (pairs quad-regions)]
    (tset ret name [])
    (each [key {: x : y : w : h : iw : ih} (pairs regions)]
      (tset (. ret name) key (love.graphics.newQuad x y w h iw ih))))
  ret)

(fn level.set-quads [map-data layer-name ...]
  (let [layer (. map-data.layers layer-name)
        {: type : data} layer]
    (tset layer :quads [])
    (match type
      :tile (let [image (or layer.image map-data.image)
                  (iw ih) (image:getDimensions)]
              (tset layer :quads (-> map-data
                                     (map.quad-regions layer-name iw ih ...)
                                     (quad-regions-to-quads))))
      :objects :nil)
    map-data))

(fn level.update-spritebatch [map-data layer-name]  
  (let [{: spritebatch : quads : data &as layer}
        (map.get-layer map-data layer-name)]
    (spritebatch:clear)
    (local tilesize map-data.tilesize)
    (each [_ {: char : x : y : bitmap-index &as d} (ipairs data)]
      (spritebatch:add (. quads char bitmap-index)
                       (* (- x 1) tilesize.x)
                       (* (- y 1) tilesize.y)))))

(fn level.add-layer [map-data layer-name ...]
  (-> map-data
      (map.add-layer layer-name ...)
      (map.deserialize map-data.object-creation-callbacks))
  (local layer (. map-data :layers layer-name))
  (when layer.atlas
    (tset layer :image (love.graphics.newImage layer.atlas)))
  (when layer.data
    (level.set-quads map-data layer-name))
  (when (= :tile layer.type)
    (level.add-spritebatch map-data layer-name))
  (values map-data layer))

;; Use a flag to indicate when something has to be reautottiled
;; and only call it once per frame at max

(fn level.add-spritebatch [map-data layer-name]
  (local layer (map.get-layer map-data layer-name))
  (local {: tw : th : image} map-data)
  (local sb (love.graphics.newSpriteBatch (or layer.image image) (* tw th)))
  (tset layer :spritebatch sb)
  (level.update-spritebatch  map-data layer-name)
  map-data)

(fn level.initialize [map-data callbacks previews]
  (local {: name : w : h : tilesize : atlas} map-data)
  (tset map-data :object-creation-callbacks callbacks)
  (tset map-data :object-preview-callbacks previews)
  (local (tw th) (values (/ w tilesize.x) (/ h tilesize.y)))
  (tset map-data :image (love.graphics.newImage atlas))
  (each [layer-name layer (pairs map-data.layers)]
    (when layer.atlas
      (tset layer :image (love.graphics.newImage layer.atlas)))
    (when layer.data
      (level.set-quads map-data layer-name)
      )
    (match layer.type
      :tile (do (level.add-spritebatch map-data layer-name)
                (each [_ d (ipairs layer.data)]                  
                       (love.event.push :edit-level
                                        :replace-tile
                                        layer-name
                                         d.brush
                                         d.x d.y
                                         map-data.tilesize)
                       ))
      :objects :nil))  
  (setmetatable map-data level-mt))

(fn level.create [name w h tilesize atlas callbacks previews]
  (local tilesize-table (if (= :table (type tilesize))
                            tilesize
                            {:x tilesize :y tilesize}))
  (local map-data (-> (map.empty name w h tilesize-table atlas)
                      (map.deserialize callbacks)))
  (level.initialize map-data callbacks previews))

level
