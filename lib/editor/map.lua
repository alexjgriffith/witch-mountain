local map = {}
local cwd = ((...):gsub("%.map$", "") .. ".")
local autotile = require((cwd .. "lib.autotile"))
local function xy_to_id(x, y, width)
  return ((x * width) + y)
end
local function zeros(x, y)
  local ret = ""
  for i = 1, y do
    for j = 1, x do
      ret = (ret .. "0")
    end
    ret = (ret .. "\n")
  end
  return ret
end
map.empty = function(name, w, h, tilesize, atlas)
  local tw = math.floor((w / tilesize.x))
  local th = math.floor((h / tilesize.y))
  return {name = name, w = w, h = h, tw = tw, th = th, atlas = atlas, tilesize = tilesize, layers = {}}
end
map["add-layer"] = function(map_data, layer, type, brushes, atlas_3f, encoded_data_3f)
  local encoded_data
  do
    local _1_, _2_ = type, encoded_data_3f
    if ((_1_ == "tile") and (_2_ == nil)) then
      encoded_data = zeros(map_data.tw, map_data.th)
    elseif ((_1_ == "objects") and (_2_ == nil)) then
      encoded_data = {}
    elseif (true and true) then
      local _ = _1_
      local _0 = _2_
      encoded_data = encoded_data_3f
    else
      encoded_data = nil
    end
  end
  local l = {type = type, brushes = brushes, atlas = atlas_3f, ["encoded-data"] = encoded_data}
  map_data.layers[layer] = l
  return map_data, l
end
map["add-brush"] = function(map_data, layer_name, brush_name, brush)
  map_data.layers[layer_name].brushes[brush_name] = brush
  return map_data, brush
end
map["set-layer-image"] = function(map_data, layer_name, image_filename)
  map_data.layers[layer_name]["image"] = image_filename
  return map_data
end
map["set-name"] = function(map_data, filename)
  map_data["name"] = filename
  return map_data, filename
end
local function clone(obj)
  local ret = {}
  for key, value in pairs(obj) do
    ret[key] = value
  end
  return ret
end
map["add-object"] = function(map_data, layer_name, x, y, brush_name, object_generation_callbacks, ...)
  local layer = map_data.layers[layer_name]
  local brush = clone(layer.brushes[brush_name])
  do end (brush)["x"] = x
  brush["y"] = y
  local obj = object_generation_callbacks[brush.name](brush, ...)
  if not layer.data then
    layer["data"] = {}
  else
  end
  table.insert(layer.data, obj)
  return map_data
end
map["add-object-encoded"] = function(map_data, layer_name, x, y, brush_name, object_generation_callbacks, ...)
  local layer = map_data.layers[layer_name]
  local brush = clone(layer.brushes[brush_name])
  do end (brush)["x"] = x
  brush["y"] = y
  local obj = object_generation_callbacks[brush.name](brush, ...)
  if not layer["encoded-data"] then
    layer["encoded-data"] = {}
  else
  end
  table.insert(layer["encoded-data"], obj:serialize())
  return map_data
end
map["remove-object"] = function(map_data, layer_name, x, y)
  local rem = {}
  local data = map_data.layers[layer_name].data
  for key, _6_ in ipairs(data) do
    local _each_7_ = _6_
    local xd = _each_7_["x"]
    local yd = _each_7_["y"]
    local wd = _each_7_["w"]
    local hd = _each_7_["h"]
    if ((x > xd) and (y > yd) and (x < (xd + hd)) and (y < (yd + hd))) then
      table.insert(rem, key)
    else
    end
  end
  if (#rem > 0) then
    local i
    do
      local t_9_ = rem
      if (nil ~= t_9_) then
        t_9_ = (t_9_)[#rem]
      else
      end
      i = t_9_
    end
    local obj = data[i]
    local brush = obj:serialize()
    table.remove(data, i)
    return map_data, brush, obj
  else
    return map_data
  end
end
local function serialize_objects(data)
  local encoded_data = {}
  for key, obj in ipairs(data) do
    table.insert(encoded_data, obj:serialize())
  end
  return encoded_data
end
local function deserialize_objects(encoded_data, image, callbacks)
  local objs = {}
  for key, obj in ipairs(encoded_data) do
    table.insert(objs, callbacks[obj.name](obj, image))
  end
  return objs
end
local function make_autotile_params(brushes)
  local params = {}
  for _, brush in pairs(brushes) do
    table.insert(params, {brush.char, autotile[brush.bitmap], brush["bitmap-w"]})
  end
  return params
end
map.deserialize = function(map_data, object_generation_callbacks)
  for layer_name, layer in pairs(map_data.layers) do
    local _12_ = layer.type
    if (_12_ == "tile") then
      local brush_map
      do
        local ret = {}
        for _, b in pairs(layer.brushes) do
          ret[b.char] = b.name
        end
        brush_map = ret
      end
      local decoded_tiles = autotile.decode(layer["encoded-data"], map_data.tw, make_autotile_params(layer.brushes))
      autotile["set-brushes"](decoded_tiles, brush_map)
      do end (layer)["data"] = decoded_tiles
    elseif (_12_ == "objects") then
      layer["data"] = deserialize_objects(layer["encoded-data"], layer.image, object_generation_callbacks)
    else
    end
  end
  return map_data
end
map["clear-encoded-data"] = function(map_data)
  for _layer_name, layer in pairs(map_data.layers) do
    layer["encoded-data"] = nil
  end
  return map_data
end
local function clear_data(map_data)
  for _layer_name, layer in pairs(map_data.layers) do
    layer["data"] = nil
  end
  return map_data
end
local function clone_encoded_layer(layer)
  local _let_14_ = layer
  local type = _let_14_["type"]
  local brushes = _let_14_["brushes"]
  local encoded_data = _let_14_["encoded-data"]
  local atlas_3f = layer.atlas
  return {type = type, brushes = brushes, ["encoded-data"] = encoded_data, atlas = atlas_3f}
end
local function clone_encoded_data(map_data)
  local _let_15_ = map_data
  local name = _let_15_["name"]
  local w = _let_15_["w"]
  local h = _let_15_["h"]
  local tw = _let_15_["tw"]
  local th = _let_15_["th"]
  local atlas = _let_15_["atlas"]
  local tilesize = _let_15_["tilesize"]
  local save_map = {name = name, w = w, h = h, tw = tw, th = th, atlas = atlas, tilesize = tilesize, layers = {}}
  for key, layer in pairs(map_data.layers) do
    save_map.layers[key] = clone_encoded_layer(layer)
  end
  return save_map
end
map.serialize = function(map_data)
  for layer_name, layer in pairs(map_data.layers) do
    local _16_ = layer.type
    if (_16_ == "tile") then
      layer["encoded-data"] = autotile.encode(layer.data)
    elseif (_16_ == "objects") then
      layer["encoded-data"] = serialize_objects(layer.data)
    else
    end
  end
  return clone_encoded_data(map_data)
end
map["replace-tile"] = function(map_data, layer_name, x, y, brush_name, reautotile_3f)
  local layer = map_data.layers[layer_name]
  local brush = clone(layer.brushes[brush_name])
  autotile["set-tile"](layer.data, x, y, map_data.tw, brush.char, brush_name)
  if reautotile_3f then
    layer["data"] = autotile["re-autotile"](layer.data, map_data.tw, brush.char, autotile[brush.bitmap], brush["bitmap-w"])
  else
  end
  return map_data
end
map["y-sort-layer"] = function(map_data, layer_name)
  local layer = map["get-layer"](map_data, layer_name)
  if ("objects" == layer.type) then
    local function _19_(a, b)
      if (a.y < b.y) then
        return true
      elseif (a.y == b.y) then
        return (a.x < b.x)
      else
        return false
      end
    end
    table.sort(layer.data, _19_)
  else
  end
  return map_data
end
map["get-layer"] = function(map_data, layer_name)
  return map_data.layers[layer_name]
end
map["get-brush"] = function(map_data, layer_name, brush_name)
  return map_data.layers[layer_name].brushes[brush_name]
end
map["get-layers"] = function(map_data)
  local ret = {}
  for layer_name, _ in pairs(map_data.layers) do
    table.insert(ret, layer_name)
  end
  return ret
end
map["get-brushes"] = function(map_data, layer_name)
  local brushes = map_data.layers[layer_name].brushes
  return brushes
end
map["quad-regions"] = function(map_data, layer_name, iw, ih, x_3f, y_3f)
  local t = {}
  local brushes = map_data.layers[layer_name].brushes
  local tilesize = map_data.tilesize
  for brush_name, brush in pairs(brushes) do
    t[brush.char] = autotile["quad-regions"](autotile["parse-bitmap"](autotile[brush.bitmap], brush["bitmap-w"]), brush["bitmap-w"], ((tilesize.x * brush.ix) + (x_3f or 0)), ((tilesize.y * brush.iy) + (y_3f or 0)), tilesize.x, tilesize.y, iw, ih)
  end
  return t
end
return map
