(local editor {})
(local brush {})

(local cwd  (.. (: ... :gsub "%.interface$" "")  "."))

(local level (require (.. cwd :level)))
;; start customization

(var pointer-colour [1 0 0 1])
(var snap-ratio 0.5)

(tset brush :brushes  [[:ground :ground :tile]
                       [:ground :chasm :tile]
                       [:objects :obj :object]])

(tset brush :count 3)

(tset brush :index 1)

(fn editor.keypressed [key code]
  (match key
    :g (brush.set 1)
    :c (brush.set 2)
    :o (brush.set 3)
    "[" (brush.decrement)
    "]" (brush.increment)))

;; end customization
(local pointer {:i 0 :j 0})
(local camera {:x 0 :y 0 :scale 1})
(var map-data nil)
(var active false)

(fn brush.set [index]
  (set brush.index index))

(fn brush.decrement []  
  (set brush.index  (math.max 1 (- brush.index 1))))

(fn brush.increment []
  (set brush.index  (math.min brush.count (+ brush.index 1))))

(fn snap [x y tilesize ratio]
  (values (* (math.floor (/ x tilesize.x ratio)) tilesize.x ratio)
          (* (math.floor (/ y tilesize.y ratio)) tilesize.y ratio)))

(var click-down false)
(fn editor.mousereleased [x y button]
  (when active
    (local [cx cy] [(- (/ x camera.scale) camera.x)
                    (- (/ y camera.scale) camera.y)])
    (local [layer-name brush-name type] (. brush :brushes brush.index))
    (local tilesize map-data.tilesize)
    (when (and (= type :tile) (= button 1))      
      (local click-up {:i (math.floor (/ cx (* tilesize.x 1)))
                       :j (math.floor (/ cy (* tilesize.y 1)))
                       :button button})
      (local end-y (math.max click-up.j click-down.j))
      (local start-y (math.min click-up.j click-down.j))
      (local end-x (math.max click-up.i click-down.i))
      (local start-x (math.min click-up.i click-down.i))
      ;; fix here      
      (for [i start-y end-y]
        (for [j start-x end-x]
          (map-data:add layer-name (+ j 1) (+ i 1) brush-name
                        (and (= i end-y) (= j end-x))) ;; autotile
          ))
      (set click-down nil)
      (values click-up camera))
    (when (and (= type :object) (= button 1))
      (local (sx sy) (snap cx cy tilesize snap-ratio))
      (map-data:add layer-name sx sy brush-name))
    
    (when (and (= type :object) (= button 2))
      (map-data:remove layer-name cx cy brush-name)
      )
    ))

(fn editor.mousepressed [x y button]  
  (when active
    (local [cx cy] [(- (/ x camera.scale) camera.x)
                    (- (/ y camera.scale) camera.y)])
    (when (= button 1)
      (local tilesize map-data.tilesize)
      (set click-down {:i (math.floor (/ cx (* tilesize.x 1)))
                       :j (math.floor (/ cy (* tilesize.y 1)))
                       :button button})
      (values click-down camera))))

(fn tile [x y tilesize]
  (values (+ (math.floor (/ x (* tilesize.x 1))) 1)
          (+ (math.floor (/ y (* tilesize.y 1))) 1)))

(fn editor.draw []  
  (when active
    (local (x y) (love.mouse.getPosition))
    (local [cx cy] [(- (/ x camera.scale) camera.x)
                    (- (/ y camera.scale) camera.y)])
    (local tilesize map-data.tilesize)
    (local [layer-name brush-name type] (. brush :brushes brush.index))
    (local (sx sy) (snap cx cy tilesize snap-ratio))
    (local (tx ty) (tile cx cy tilesize))
    (love.graphics.push)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.scale camera.scale)
    (love.graphics.translate camera.x camera.y)
    (match type
      :object (map-data:preview layer-name sx sy brush-name type)
      :tile (map-data:preview layer-name tx ty brush-name type))
    (love.graphics.pop)
    ;; (love.graphics.push)
    ;; (love.graphics.setColor pointer-colour)  
    ;; (match type
    ;;   :object (do
    ;;             (love.graphics.rectangle
    ;;              :fill
    ;;              (* (+ sx camera.x) camera.scale)
    ;;              (* (+ sy camera.y) camera.scale)                 
    ;;              (* snap-ratio tilesize camera.scale)
    ;;              (* snap-ratio tilesize camera.scale))
    ;;             ))
    ;; (love.graphics.pop)
    ;; (map-data:draw)
    ;; Draw Pointer
    (love.graphics.push)
    (local tilesize map-data.tilesize)
    (love.graphics.setColor pointer-colour)
    (love.graphics.rectangle "line"
                             (* (+ (* tilesize.x pointer.i) camera.x) camera.scale)
                             (* (+ (* tilesize.y pointer.j) camera.y) camera.scale)
                             (* camera.scale tilesize.x)
                             (* camera.scale tilesize.y)
                             20)
  (love.graphics.pop)
  (love.graphics.setColor 1 1 1 1)))

(fn editor.update-camera [{: x : y : scale}]  
  (set camera.x (or x 0))
  (set camera.y (or y 0))
  (set camera.scale (or scale 1))
  camera)

(fn editor.update-pointer []
  (local (ix iy) (love.mouse.getPosition))
  ;; offsets can be replaced with a global
  (local offsets {:gx 0 :s 1 :gy 0})
  (local [x y]
         [ (/ (- ix offsets.gx) offsets.s) (/ (- iy offsets.gy) offsets.s)])
  (local tilesize map-data.tilesize)
  (tset pointer :i  (math.floor (/ (- (/ x camera.scale) camera.x) tilesize.x)))
  (tset pointer :j  (math.floor (/ (- (/ y camera.scale) camera.y) tilesize.y))))

(fn editor.set-level [new-level]
  (set map-data new-level))

(fn editor.active [] active)

(fn editor.toggle []
  (set active (not active)))

(fn editor.update [dt camera?]
  (when active        
    (when camera?
      (editor.update-camera camera?))
    (editor.update-pointer)))

(fn editor.init [new-map new-camera new-pointer-colour?]
  (set active true)
  (set map-data new-map)
  (when new-pointer-colour?
    (set pointer-colour new-pointer-colour?))
  (editor.update-camera camera new-camera))

{: editor : brush}
