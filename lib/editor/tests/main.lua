-- bootstrap the compiler
fennel = require("fennel")
table.insert(package.loaders, fennel.make_searcher({correlate=true}))

fennel.path = love.filesystem.getSource() .. "/?.fnl;" ..   
   fennel.path

pp = function(x) print(fennel.view(x)) end

require("test-level")
