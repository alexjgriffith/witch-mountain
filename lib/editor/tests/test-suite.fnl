(local debuging true)
(local fennel (require :fennel))


(fn db [note str]
  (when debuging ;; t0x02
    `(print ,note (fennel.view ,str))))

(fn mock [name fun]
  (when debuging
    `(local ,name ,fun)))

(fn test [name fun comp? result]
  (when debuging
    `(let       
         [comp#
          (or (and (~= ,comp? :=) ,comp?)
              (fn [ga# gb#]                          
                (= (fennel.view ga#) (fennel.view gb#))))
          result# ,fun]
       (print  (fennel.view
               [,name (if (comp# result# ,result)
                          :pass
                          (values :fail result# ,result))])))))

{: test : mock : db}
