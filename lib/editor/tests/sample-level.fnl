{:atlas "editor/assets/atlas.png"
 :h 256
 :layers {:ground {:atlas "editor/assets/atlas.png"
                   :brushes {:chasm {:bitmap "bitmap-1"
                                     :bitmap-w 1
                                     :char "0"
                                     :ix 4
                                     :iy 4
                                     :name "chasm"}
                             :ground {:bitmap "bitmap-3x3-simple-alt"
                                      :bitmap-w 4
                                      :char "1"
                                      :ix 0
                                      :iy 0
                                      :name "ground"}}
                   :encoded-data "0000000000000000
0000000000000000
0011100000000000
0011100000000000
0011000000000000
0011100000000000
0001100000000000
0000000000000000
0000000000000000
0000000000000000
0000000000000000
0000000000000000
0000000000000000
0000000000000000
0000000000000000
0000000000000000"
                   :type "tile"}
          :objects {:brushes {:obj {:ch 8
                                    :cw 8
                                    :cx 4
                                    :cy 8
                                    :h 32
                                    :ih 256
                                    :iw 256
                                    :name "obj"
                                    :ph 32
                                    :pw 32
                                    :px 128
                                    :py 32
                                    :w 32}
                              :player {:ch 8
                                       :cw 8
                                       :cx 4
                                       :cy 8
                                       :h 16
                                       :ih 256
                                       :iw 256
                                       :name "player"
                                       :ph 16
                                       :pw 16
                                       :px 128
                                       :py 16
                                       :w 16}}
                    :encoded-data [{:ch 8
                                    :cw 8
                                    :cx 4
                                    :cy 8
                                    :h 16
                                    :ih 256
                                    :iw 256
                                    :name "player"
                                    :ph 16
                                    :pw 16
                                    :px 128
                                    :py 16
                                    :w 16
                                    :x 2
                                    :y 2}
                                   {:ch 8
                                    :cw 8
                                    :cx 4
                                    :cy 8
                                    :h 32
                                    :ih 256
                                    :iw 256
                                    :name "obj"
                                    :ph 32
                                    :pw 32
                                    :px 128
                                    :py 32
                                    :w 32
                                    :x 100
                                    :y 30}]
                    :type "objects"}}
 :name "test"
 :th 16
 :tilesize {:x 16 :y 16}
 :tw 16
 :w 256}
