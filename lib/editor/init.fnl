(local editor {:__TITLE "editor.fnl"
                     :__DESCRIPTION "A level editor for love"
                     :__AUTHOR "AlexJGriffith"
                     :__VERSION "0.1.0"
                     :__LICENCE "GPL3+"
                     })

(local cwd  (.. (: ... :gsub "%.init$" "")  "."))
(local level (require (.. cwd :level)))
(local {: brush : editor} (require (.. cwd :interface)))

(print "loading editor - main.fnl")

(tset editor :brush brush)
(tset level :editor editor)

level
