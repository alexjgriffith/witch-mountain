(local timers {:__TITLE "timers"
              :__DESCRIPTION "An timer container library"
              :__AUTHOR "AlexJGriffith"
              :__VERSION "0.1.0"
              :__LICENCE "GPL3+"
              })

(local timer {})

(macro test [name fun comp result]
  (local t0x01 false)
  (when t0x01 ;; Set to false when distributing
    `(do
       (local fennel# (require :fennel))
       (print (fennel#.view
               [,name (if (,comp ,fun ,result)
                          :pass
                          (values :fail ,fun ,result))])))))

(fn timer.over [self]
  (set self.active false)
  (set self.time 0)
  self)

(fn timer.reset [self]
  (set self.time 0)
  self)

(fn timer.update [self dt functions]
  (if self.active
      (do
          (set self.time (+ self.time dt))
          (if (> self.time self.end)
              (do (if self.restart
                      (self:reset)
                      (self:over))
                  (when self.callback
                    (if (= (type self.callback) :string)
                        ((. functions self.callback))
                        (self.callback)))
                  (values self true :over))
              (values self true :running)))
      (values self false :stoped)))

(fn timer.start [self]
  (set self.active true)
  self)

(fn timer.pause [self]
  (set self.active false)
  self)

(fn timer.set-restart [self restart]
  (set self.restart restart)
  self)

(fn timer.set-end [self end]
  (set self.end end)
  self)

(fn timer.__tostring [{: time : end : restart : active : callback}]
  (.. :#Timer: time :/ end
      (if (or restart active callback) "-")
      (if restart "R" "")
      (if active "A" "")
      (if callback (.. "(" callback ")") "")))

(local timer-mt
       {:__index timer
        :__tostring timer.__tostring
        })

(fn timer.eq [timerA timerB]
  (and (= timerA.time timerB.time)
       (= timerA.end timerB.end)
       (= timerA.callback timerB.callback)
       (= timerA.restart timerB.restart)
       (= timerA.active timerB.active)))

(fn timer.new [end callback? start? restart?]
  (setmetatable
   {:time 0
    :end end
    :callback (or callback? false)
    :restart (or restart? false)
    :active (or start? false)}
   timer-mt))

(test "create-timer" (timer.new 10 :test true true) timer.eq
      {:time 0 :end 10 :callback :test :restart true :active true})

(test "timer.over" (-> (timer.new 10 :test true true)
                       (timer.update 2)
                       timer.over)
      timer.eq
      {:time 0 :end 10 :callback :test :restart true :active false})
(test "timer.update" (-> (timer.new 10 :test true true)
                         (timer.update 2))
      timer.eq
      {:time 2 :end 10 :callback :test :restart true :active true})
(test "timer.reset" (-> (timer.new 10 :test true true)
                         (timer.update 2)
                         timer.reset)
      timer.eq
      {:time 0 :end 10 :callback :test :restart true :active true})
(test "timer.start" (-> (timer.new 10 :test false true)
                         (timer.start))
      timer.eq
      {:time 0 :end 10 :callback :test :restart true :active true})
(test "timer.pause" (-> (timer.new 10 :test true true)
                        (timer.pause))
      timer.eq
      {:time 0 :end 10 :callback :test :restart true :active false})
(test "timer.set-reset" (-> (timer.new 10 :test true true)
                            (timer.set-restart false))
      timer.eq
      {:time 0 :end 10 :callback :test :restart false :active true})
(test "timer.set-end" (-> (timer.new 10 :test true true)
                          (timer.set-end 2))
      timer.eq
      {:time 0 :end 2 :callback :test :restart true :active true})
(test "timer.__tostring" (let [t (timer.new 10 :test true true)]
                           (t:__tostring))
      =
      "#Timer:0/10-RA(test)")

(fn timers.add [self name ...]
  (local new-timer (timer.new ...))
  (tset self.timers name new-timer)
  (values new-timer true))

(fn timers.get [self name]
  (local ret (. self :timers name))
  (values ret (if ret true false)))

(fn timers.drop [self name]
  (local ret (. self.timers name))
  (tset self.timers name nil)
  (values ret true))

(fn timers.update [self dt]
  (each [name timer (pairs self.timers)]
    (timer:update dt self.functions)))

(fn timers.__tostring [self]
    (each [name timer (pairs self.timers)]
      (timer:__tostring)))

(local timers-mt
       {:__index timers})
        
(fn timers.initialize [functions]
  (setmetatable {: functions
                 :timers {}}
                timers-mt))

(test "timers.add.update.get"
      (let [tc (timers.initialize [])]
        (tc:add :test 10 :callback true true)
        (tc:update 5)
        (local t (tc:get :test))
        (t:__tostring))
      =
      "#Timer:5/10-RA(callback)")

(test "timers.add.update.get(big)"
      (let [tc (timers.initialize {:callback (fn [])})]
        (tc:add :test 10 :callback true true)
        (tc:update 11)
        (local t (tc:get :test))
        (t:__tostring))
      =
      "#Timer:0/10-RA(callback)")

(test "timers.add.update.get(big-noreset)"
      (let [tc (timers.initialize {:callback (fn [])})]
        (tc:add :test 10 :callback true false)
        (tc:update 11)
        (local t (tc:get :test))
        (t:__tostring))
      =
      "#Timer:0/10-(callback)")

timers
