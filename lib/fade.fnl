(var fade [:in 0])

(local wipecanvas (love.graphics.newCanvas 128 96))

(var colour [1 1 1 1])

(fn set-colour [c]
  (set colour c))

(fn draw-fade [r]
  (love.graphics.setCanvas wipecanvas)
  (love.graphics.clear)
  (love.graphics.push)
  (love.graphics.setColor colour)
  (love.graphics.rectangle :fill 0 0 128 96)
  (love.graphics.setBlendMode :replace)
  (love.graphics.setColor 1 1 1 0)
  (love.graphics.circle :fill 64 48 r)
  (love.graphics.setBlendMode :alpha)
  (love.graphics.pop)
  (love.graphics.setCanvas))

(var out-callback? nil)
(var in-callback? nil)
(fn fade-out [callback]
  (set out-callback? callback)
  (set fade [:out 64]))

(fn fade-in [callback]
  (set in-callback? callback)
  (set fade [:in 0]))


(fn update [dt]
    (match fade
    [:in x]
    (do
      (tset fade 2 (+ (* 200 dt) (. fade 2)))
      (draw-fade x)
      (if (> x 64)
          (set fade [:null 64])
          (when in-callback?
            (in-callback?))))
    [:out x]
    (do
      (tset fade 2 (- (. fade 2) (* 200 dt)))
      (draw-fade x)
      (when (< x 0)
        (set fade [:null 0])
        (when out-callback?
            (out-callback?))))
    [:null y] (do
                (love.graphics.setCanvas wipecanvas)
                (love.graphics.clear)
                (love.graphics.setCanvas))))

{: update  :out fade-out :in fade-in :canvas wipecanvas : set-colour }
